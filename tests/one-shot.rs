//! Tests the one-shot modifiers and one-shot layer changes.

mod utils;

use keebit::{hid::*, prelude::*};
use utils::{assert_reports_eq, report};

const ROW: usize = 1;
const COL: usize = 3;

fn layout() -> Layout<NoTimer, (), 0, 0, 2, ROW, COL> {
    const LAYOUT: [[[MaybeAction; 3]; 1]; 2] = [
        [[k(A), ml1(RIGHT_ALT, 1), l1(1)]], // L0
        [[k(X), k(Y), m1(LEFT_CTRL)]],
    ];

    Layout::builder((), &LAYOUT)
        .with_below(|layer| match layer {
            2 => Some(0),
            _ => None,
        })
        .build()
        .unwrap()
}

#[test]
fn simple_layer_change() {
    let mut layout = layout();

    assert_eq!(layout.state().active_layers.active(), 0);

    // One shot move to layer 1:
    assert_reports_eq(layout.reports([[false, false, true]]), &[report()]);
    assert_reports_eq(layout.reports([[false, false, false]]), &[report()]);

    assert_eq!(layout.state().active_layers.active(), 1);

    // First time, this is a X:
    assert_reports_eq(
        layout.reports([[true, false, false]]),
        &[report().with_key(X)],
    );
    assert_reports_eq(layout.reports([[false, false, false]]), &[report()]);

    assert_eq!(layout.state().active_layers.active(), 0);

    // Second time, this is a A:
    assert_reports_eq(
        layout.reports([[true, false, false]]),
        &[report().with_key(A)],
    );
    assert_reports_eq(layout.reports([[false, false, false]]), &[report()]);
}

#[test]
#[ignore = "not sure how to handle a second tap on the same OS key"]
fn layer_change_then_modifier() {
    let mut layout = layout();

    assert_eq!(layout.state().active_layers.active(), 0);

    // One shot move to layer 1:
    assert_reports_eq(layout.reports([[false, false, true]]), &[report()]);
    assert_reports_eq(layout.reports([[false, false, false]]), &[report()]);

    assert_eq!(layout.state().active_layers.active(), 1);

    // One shot activation of the modifier:
    assert_reports_eq(
        layout.reports([[false, false, true]]),
        &[report().with_mod(LeftCtrl)],
    );
    assert_reports_eq(
        layout.reports([[false, false, false]]),
        &[report().with_mod(LeftCtrl)],
    );

    assert_eq!(layout.state().active_layers.active(), 0);

    // When we press A, the mod apply, but when we release it, the mod is released as well:
    assert_reports_eq(
        layout.reports([[true, false, false]]),
        &[report().with_mod(LeftCtrl).with_key(A)],
    );
    assert_reports_eq(layout.reports([[false, false, false]]), &[report()]);
}
