mod utils;

use keebit::{hid::LeftShift, prelude::*};
use std::{cell::Cell, sync::Arc};
use utils::{assert_reports_eq, report, LayoutDebouncer};

const ROW: usize = 2;
const COL: usize = 3;

struct MyMonotonicTime(Arc<Cell<u64>>);

impl MonotonicTimer for MyMonotonicTime {
    fn now(&mut self) -> u64 {
        self.0.get()
    }
}

/* Tests */

fn layout() -> LayoutDebouncer<MyMonotonicTime, (), 4, 2, 2, ROW, COL> {
    const LAYOUT: [[[MaybeAction; 3]; 2]; 2] = [
        [[k(A), k(B), k(C)], [k(D), k(E), XXXXX]],
        [[k(V), k(W), k(X)], [k(Comma), k(Dot), m(LEFT_SHIFT)]],
    ];

    let now = Default::default();
    let layout = Layout::builder((), &LAYOUT)
        .with_timer(MyMonotonicTime(Arc::clone(&now)))
        .with_debounce_time(25)
        .with_chords::<4, 2>([
            (&[(0, 0), (0, 1)], l(1)),
            (&[(1, 0), (1, 1)], k(Z)),
            (&[(1, 1), (1, 2)], km(Y, LEFT_SHIFT)),
            (&[(0, 0), (0, 2)], hold(k(P), k(Q))),
        ])
        .build()
        .unwrap();

    LayoutDebouncer::new(layout, now)
}

#[test]
fn regular_key_press() {
    let mut layout = layout();

    // A single key change returns the appropriate key:
    assert_reports_eq(
        layout.reports([[false, false, false], [true, false, false]]),
        &[report().with_key(D)],
    );

    // The release returns an empty report:
    assert_reports_eq(
        layout.reports([[false, false, false], [false, false, false]]),
        &[report()],
    );
}

#[test]
fn chord_press() {
    let mut layout = layout();

    // Press 2 keys at once to activate the chord:
    assert_reports_eq(
        layout.reports([[false, false, false], [true, true, false]]),
        &[report().with_key(Z)],
    );

    // Now we release one key. The chord should still be ongoing:
    assert_reports_eq(
        layout.reports([[false, false, false], [true, false, false]]),
        &[report().with_key(Z)],
    );

    // Now we release the remaining key. The chord should not be ongoing:
    assert_reports_eq(
        layout.reports([[false, false, false], [false, false, false]]),
        &[report()],
    );
}

#[test]
fn chord_presses_using_a_common_key_all_released_at_once() {
    let mut layout = layout();

    // Press 2 keys at once to activate the chord:
    assert_reports_eq(
        layout.reports([[false, false, false], [true, true, false]]),
        &[report().with_key(Z)],
    );

    // Now we release one key. The chord should still be ongoing:
    assert_reports_eq(
        layout.reports([[false, false, false], [true, false, false]]),
        &[report().with_key(Z)],
    );

    // Let's press a new chord:
    assert_reports_eq(
        layout.reports([[false, false, false], [true, true, true]]),
        &[report().with_key(Z).with_mod(LeftShift).with_key(Y)],
    );

    // Now we release the keys. Both chords should be cancelled:
    assert_reports_eq(
        layout.reports([[false, false, false], [false, false, false]]),
        &[report()],
    );
}

#[test]
fn chord_presses_using_a_common_key_first_chord_released_first() {
    let mut layout = layout();

    // Press 2 keys at once to activate the chord:
    assert_reports_eq(
        layout.reports([[false, false, false], [true, true, false]]),
        &[report().with_key(Z)],
    );

    // Now we release one key. The chord should still be ongoing:
    assert_reports_eq(
        layout.reports([[false, false, false], [true, false, false]]),
        &[report().with_key(Z)],
    );

    // Let's press a new chord:
    assert_reports_eq(
        layout.reports([[false, false, false], [true, true, true]]),
        &[report().with_key(Z).with_mod(LeftShift).with_key(Y)],
    );

    // Now we release only the first chord:
    assert_reports_eq(
        layout.reports([[false, false, false], [false, true, true]]),
        &[report().with_mod(LeftShift).with_key(Y)],
    );
}

#[test]
fn keep_chord_pressed_and_press_a_key_from_the_chord_then_release_chord_first() {
    let mut layout = layout();

    // Press 2 keys at once to activate the chord:
    assert_reports_eq(
        layout.reports([[false, false, false], [true, true, false]]),
        &[report().with_key(Z)],
    );

    // Now we release one key. The chord should still be ongoing:
    assert_reports_eq(
        layout.reports([[false, false, false], [true, false, false]]),
        &[report().with_key(Z)],
    );

    // Let's press a key belonging to the chord:
    assert_reports_eq(
        layout.reports([[false, false, false], [true, true, false]]),
        &[report().with_key(Z).with_key(E)],
    );

    // Now we release only the chord:
    assert_reports_eq(
        layout.reports([[false, false, false], [false, true, false]]),
        &[report().with_key(E)],
    );

    // Then release the key:
    assert_reports_eq(
        layout.reports([[false, false, false], [false, false, false]]),
        &[report()],
    );
}

#[test]
fn keep_chord_pressed_and_press_a_key_from_the_chord_then_release_key_first() {
    let mut layout = layout();

    // Press 2 keys at once to activate the chord:
    assert_reports_eq(
        layout.reports([[false, false, false], [true, true, false]]),
        &[report().with_key(Z)],
    );

    // Now we release one key. The chord should still be ongoing:
    assert_reports_eq(
        layout.reports([[false, false, false], [true, false, false]]),
        &[report().with_key(Z)],
    );

    // Let's press a key belonging to the chord:
    assert_reports_eq(
        layout.reports([[false, false, false], [true, true, false]]),
        &[report().with_key(Z).with_key(E)],
    );

    // Now we release only the key:
    assert_reports_eq(
        layout.reports([[false, false, false], [true, false, false]]),
        &[report().with_key(Z)],
    );

    // Then release the chord:
    assert_reports_eq(
        layout.reports([[false, false, false], [false, false, false]]),
        &[report()],
    );
}

#[test]
fn use_chord_to_change_layer() {
    let mut layout = layout();

    // Press 2 keys at once to use the alt layer. No state change for now:
    assert_reports_eq(
        layout.reports([[true, true, false], [false, false, false]]),
        &[report()],
    );

    // Now we press one key on the alt layer:
    assert_reports_eq(
        layout.reports([[true, false, true], [false, false, false]]),
        &[report().with_key(X)],
    );

    // Let's release the layer chord:
    assert_reports_eq(
        layout.reports([[false, false, true], [false, false, false]]),
        &[report().with_key(X)],
    );

    // Now we release the key:
    assert_reports_eq(
        layout.reports([[false, false, false], [false, false, false]]),
        &[report()],
    );
}

#[test]
fn use_a_mod_on_a_layer_activated_by_a_chord() {
    let mut layout = layout();

    // Press 2 keys at once to use the alt layer. No state change for now:
    assert_reports_eq(
        layout.reports([[true, true, false], [false, false, false]]),
        &[report()],
    );

    // Now we press the modifier on the alt layer:
    assert_reports_eq(
        layout.reports([[true, false, false], [false, false, true]]),
        &[report().with_mod(LeftShift)],
    );

    // Let's release the layer chord:
    assert_reports_eq(
        layout.reports([[false, false, false], [false, false, true]]),
        &[report().with_mod(LeftShift)],
    );

    // Now we press a key on the main layer while keeping the modifier active:
    assert_reports_eq(
        layout.reports([[true, false, false], [false, false, true]]),
        &[report().with_mod(LeftShift).with_key(A)],
    );

    // Release the key:
    assert_reports_eq(
        layout.reports([[false, false, false], [false, false, true]]),
        &[report().with_mod(LeftShift)],
    );

    // Now we press a a chord while keeping the modifier active:
    assert_reports_eq(
        layout.reports([[false, false, false], [true, true, true]]),
        &[report().with_mod(LeftShift).with_key(Z)],
    );

    // Release the mod:
    assert_reports_eq(
        layout.reports([[false, false, false], [true, false, false]]),
        &[report().with_key(Z)],
    );

    // Release the chord:
    assert_reports_eq(
        layout.reports([[false, false, false], [false, false, false]]),
        &[report()],
    );
}

#[test]
fn chord_is_a_taphold_action() {
    let mut layout = layout();

    // A single press returns nothing:
    assert_reports_eq(
        layout.reports([[true, false, true], [false, false, false]]),
        &[report()],
    );

    // Now, we press another key, without releasing the chord:
    assert_reports_eq(
        layout.reports([[true, false, true], [true, false, false]]),
        &[report().with_key(P), report().with_key(P).with_key(D)],
    );

    // Release the chord:
    assert_reports_eq(
        layout.reports([[false, false, false], [true, false, false]]),
        &[report().with_key(D)],
    );
}
