#![cfg(feature = "reserved")]

mod utils;

use keebit::{hid::*, prelude::*};
use utils::{assert_reports_eq, report};

const ONE: ReservedByte = ReservedByte(1);

fn layout() -> Layout<NoTimer, (), 0, 0, 1, 1, 3> {
    Layout::builder((), &[[[m(ONE), m(LeftAlt + ONE), k(A)]]])
        .build()
        .unwrap()
}

#[test]
fn mac_fn_is_recognized() {
    let mut layout = layout();

    assert_reports_eq(
        layout.reports([[true, false, false]]),
        &[report().with_reserved_bits(ONE)],
    );
}

#[test]
fn mac_fn_is_applied() {
    let mut layout = layout();

    assert_reports_eq(
        layout.reports([[false, true, false]]),
        &[report().with_mod(LeftAlt).with_reserved_bits(ONE)],
    );

    assert_reports_eq(
        layout.reports([[false, true, true]]),
        &[report()
            .with_mod(LeftAlt)
            .with_reserved_bits(ONE)
            .with_key(A)],
    );
}

#[test]
fn all_combinations_pass() {
    let several_mods = LeftAlt + LeftCtrl;
    let several_res = ONE + ONE;

    let _ = Layout::builder(
        (),
        &[[[
            // Modifiers:
            m(ONE),
            m(ONE + LeftAlt),
            m(LeftAlt + ONE),
            m(ONE + ONE),
            m(LeftAlt + LeftCtrl),
            m(ONE + several_mods),
            m(several_mods + ONE),
            m(several_res + ONE),
            m(ONE + several_res),
            m(several_mods + LeftCtrl),
            m(LEFT_SHIFT + several_mods),
            // One shot modifiers:
            m1(ONE),
            m1(ONE + LeftAlt),
            m1(LeftAlt + ONE),
            m1(ONE + ONE),
            m1(LeftAlt + LeftCtrl),
            m1(ONE + several_mods),
            m1(several_mods + ONE),
            m1(several_res + ONE),
            m1(ONE + several_res),
            m1(several_mods + LeftCtrl),
            m1(LEFT_SHIFT + several_mods),
            // Modifiers + layer change:
            lm(1, ONE),
            lm(1, ONE + LeftAlt),
            lm(1, LeftAlt + ONE),
            lm(1, ONE + ONE),
            lm(1, LeftAlt + LeftCtrl),
            lm(1, ONE + several_mods),
            lm(1, several_mods + ONE),
            lm(1, several_res + ONE),
            lm(1, ONE + several_res),
            lm(1, several_mods + LeftCtrl),
            lm(1, LEFT_SHIFT + several_mods),
            // One shot Modifiers + layer change:
            lm1(1, ONE),
            lm1(1, ONE + LeftAlt),
            lm1(1, LeftAlt + ONE),
            lm1(1, ONE + ONE),
            lm1(1, LeftAlt + LeftCtrl),
            lm1(1, ONE + several_mods),
            lm1(1, several_mods + ONE),
            lm1(1, several_res + ONE),
            lm1(1, ONE + several_res),
            lm1(1, several_mods + LeftCtrl),
            lm1(1, LEFT_SHIFT + several_mods),
            // Keypress:
            k(ONE + Dot),
            k(ONE + LeftAlt + Dot),
            k(LeftAlt + ONE + Dot),
            k(ONE + ONE + Dot),
            k(LeftAlt + LeftCtrl + Dot),
            k(ONE + several_mods + Dot),
            k(several_mods + ONE + Dot),
            k(several_res + ONE + Dot),
            k(ONE + several_res + Dot),
            k(several_mods + LeftCtrl + Dot),
            k(LEFT_SHIFT + several_mods + Dot),
            k(ONE + Dot + Comma),
            k(ONE + LeftAlt + Dot + Comma),
            k(LeftAlt + ONE + Dot + Comma),
            k(ONE + ONE + Dot + Comma),
            k(LeftAlt + LeftCtrl + Dot + Comma),
            k(ONE + several_mods + Dot + Comma),
            k(several_mods + ONE + Dot + Comma),
            k(several_res + ONE + Dot + Comma),
            k(ONE + several_res + Dot + Comma),
            k(several_mods + LeftCtrl + Dot + Comma),
            k(LEFT_SHIFT + several_mods + Dot + Comma),
            k(LEFT_SHIFT + several_mods + A + B + C + D + E + F),
        ]]],
    )
    .build();
}
