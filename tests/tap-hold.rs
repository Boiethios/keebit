mod utils;

use keebit::{hid::LeftShift, prelude::*};
use std::{cell::Cell, sync::Arc};
use test_case::test_case;
use utils::{assert_reports_eq, report};

const ROW: usize = 2;
const COL: usize = 3;

struct MyMonotonicTime(Arc<Cell<u64>>);

impl MonotonicTimer for MyMonotonicTime {
    fn now(&mut self) -> u64 {
        self.0.get()
    }
}

fn layout(
    permissive_mod_for_tap_action: bool,
    preemptive_mod_hold: bool,
) -> (
    Arc<Cell<u64>>,
    Layout<MyMonotonicTime, (), 0, 0, 1, ROW, COL>,
) {
    const LAYOUT: [[[MaybeAction; 3]; 2]; 1] = [[
        [
            k(A),
            hold(k(B), m(LEFT_CTRL)),
            hold(k(C), km(C, LEFT_SHIFT)),
        ],
        [hold(k(D), km(D, LEFT_SHIFT)), k(E), m(LEFT_SHIFT)],
    ]];

    let now = Default::default();
    let layout = Layout::builder((), &LAYOUT)
        .with_timer(MyMonotonicTime(Arc::clone(&now)))
        .with_permissive_mod_for_tap_action(permissive_mod_for_tap_action)
        .with_preemptive_mod_hold(preemptive_mod_hold)
        .build()
        .unwrap();

    (now, layout)
}

#[test_case(false, false)]
#[test_case(false, true)]
#[test_case(true, false)]
#[test_case(true, true)]
fn key_tapped(permissive_mod_for_tap_action: bool, preemptive_mod_hold: bool) {
    let (_now, mut layout) = layout(permissive_mod_for_tap_action, preemptive_mod_hold);

    // Key pressed: no report change without a timeout:
    assert_reports_eq(
        layout.reports([[false, false, false], [true, false, false]]),
        &[report()],
    );

    // Key released: this is a tap, so we get the report with the key + the report to “cancel” it:
    assert_reports_eq(
        layout.reports([[false, false, false], [false, false, false]]),
        &[report().with_key(D), report()],
    );
}

#[test_case(false, false)]
#[test_case(false, true)]
#[test_case(true, false)]
#[test_case(true, true)]
fn key_held(permissive_mod_for_tap_action: bool, preemptive_mod_hold: bool) {
    let (now, mut layout) = layout(permissive_mod_for_tap_action, preemptive_mod_hold);

    // Key pressed: no report change without a timeout:
    assert_reports_eq(
        layout.reports([[false, false, false], [true, false, false]]),
        &[report()],
    );

    now.set(1000);

    // When the timeout has elapsed, it is a hold:
    assert_reports_eq(
        layout.reports([[false, false, false], [true, false, false]]),
        &[report().with_key(D).with_mod(LeftShift)],
    );

    // Now we release the key, the state goes back to empty:
    assert_reports_eq(
        layout.reports([[false, false, false], [false, false, false]]),
        &[report()],
    );
}

#[test_case(false, false)]
#[test_case(false, true)]
#[test_case(true, false)]
#[test_case(true, true)]
fn two_consecutive_interruptions_tap_hold_key(
    permissive_mod_for_tap_action: bool,
    preemptive_mod_hold: bool,
) {
    let (_now, mut layout) = layout(permissive_mod_for_tap_action, preemptive_mod_hold);

    // Key C pressed: no report change without a timeout:
    assert_reports_eq(
        layout.reports([[false, false, true], [false, false, false]]),
        &[report()],
    );

    // Key D pressed without a timeout, C is a tap:
    assert_reports_eq(
        layout.reports([[false, false, true], [true, false, false]]),
        &[report().with_key(C)],
    );

    // Key E pressed without a timeout, this is a tap again:
    assert_reports_eq(
        layout.reports([[false, false, false], [true, true, false]]),
        &[report().with_key(D), report().with_key(D).with_key(E)],
    );
}

#[test_case(false, false)]
#[test_case(false, true)]
#[test_case(true, false)]
#[test_case(true, true)]
fn tap_two_keys_after_a_mod(permissive_mod_for_tap_action: bool, preemptive_mod_hold: bool) {
    let (_now, mut layout) = layout(permissive_mod_for_tap_action, preemptive_mod_hold);

    // Activate the mod:
    assert_reports_eq(
        layout.reports([[false, false, false], [false, false, true]]),
        &[report().with_mod(LeftShift)],
    );

    // Tap a key:
    assert_reports_eq(
        layout.reports([[false, false, true], [false, false, true]]),
        &[report().with_mod(LeftShift)],
    );
    assert_reports_eq(
        layout.reports([[false, false, false], [false, false, true]]),
        &[
            report().with_key(C).with_mod(LeftShift),
            report().with_mod(LeftShift),
        ],
    );

    // Tap another key:
    assert_reports_eq(
        layout.reports([[false, false, false], [true, false, true]]),
        &[report().with_mod(LeftShift)],
    );
    assert_reports_eq(
        layout.reports([[false, false, false], [false, false, true]]),
        &[
            report().with_key(D).with_mod(LeftShift),
            report().with_mod(LeftShift),
        ],
    );
}

mod permissive_mod_for_tap_action {
    use super::*;
    use keebit::hid::LeftShift;
    use test_case::test_case;

    #[test_case(false)]
    #[test_case(true)]
    fn works_with_key_release(preemptive_mod_hold: bool) {
        let (_now, mut layout) = layout(true, preemptive_mod_hold);

        // First, let's activate the mod:
        assert_reports_eq(
            layout.reports([[false, false, false], [false, false, true]]),
            &[report().with_mod(LeftShift)],
        );

        // Key pressed: no report change without a timeout:
        assert_reports_eq(
            layout.reports([[false, false, false], [true, false, true]]),
            &[report().with_mod(LeftShift)],
        );

        // Realease the modifier:
        assert_reports_eq(
            layout.reports([[false, false, false], [true, false, false]]),
            &[report()],
        );

        // Now we release the key, the mod still apply, thanks to `permissive_mod_for_tap_action`:
        assert_reports_eq(
            layout.reports([[false, false, false], [false, false, false]]),
            &[report().with_mod(LeftShift).with_key(D), report()],
        );
    }

    #[test_case(false)]
    #[test_case(true)]
    fn works_with_other_key_press(preemptive_mod_hold: bool) {
        let (_now, mut layout) = layout(true, preemptive_mod_hold);

        // First, let's activate the mod:
        assert_reports_eq(
            layout.reports([[false, false, false], [false, false, true]]),
            &[report().with_mod(LeftShift)],
        );

        // Key pressed: no report change without a timeout:
        assert_reports_eq(
            layout.reports([[false, false, false], [true, false, true]]),
            &[report().with_mod(LeftShift)],
        );

        // Realease the modifier:
        assert_reports_eq(
            layout.reports([[false, false, false], [true, false, false]]),
            &[report()],
        );

        // Now we press another key, triggering the tap for the previous one.
        // The mod still apply, thanks to `permissive_mod_for_tap_action`:
        assert_reports_eq(
            layout.reports([[true, false, false], [true, false, false]]),
            &[
                report().with_mod(LeftShift).with_key(D),
                report().with_key(D).with_key(A),
            ],
        );

        assert_reports_eq(
            layout.reports([[true, false, false], [false, false, false]]),
            &[report().with_key(A)],
        );
    }

    #[test_case(false)]
    #[test_case(true)]
    fn mod_does_not_apply_when_option_is_turned_off_with_key_release(preemptive_mod_hold: bool) {
        let (_now, mut layout) = layout(false, preemptive_mod_hold);

        // First, let's activate the mod:
        assert_reports_eq(
            layout.reports([[false, false, false], [false, false, true]]),
            &[report().with_mod(LeftShift)],
        );

        // Key pressed: no report change without a timeout:
        assert_reports_eq(
            layout.reports([[false, false, false], [true, false, true]]),
            &[report().with_mod(LeftShift)],
        );

        // Realease the modifier:
        assert_reports_eq(
            layout.reports([[false, false, false], [true, false, false]]),
            &[report()],
        );

        // Now we release the key, the mod does not apply:
        assert_reports_eq(
            layout.reports([[false, false, false], [false, false, false]]),
            &[report().with_key(D), report()],
        );
    }

    #[test_case(false)]
    #[test_case(true)]
    fn mod_does_not_apply_when_option_is_turned_off_with_other_key_press(
        preemptive_mod_hold: bool,
    ) {
        let (_now, mut layout) = layout(false, preemptive_mod_hold);

        // First, let's activate the mod:
        assert_reports_eq(
            layout.reports([[false, false, false], [false, false, true]]),
            &[report().with_mod(LeftShift)],
        );

        // Key pressed: no report change without a timeout:
        assert_reports_eq(
            layout.reports([[false, false, false], [true, false, true]]),
            &[report().with_mod(LeftShift)],
        );

        // Realease the modifier:
        assert_reports_eq(
            layout.reports([[false, false, false], [true, false, false]]),
            &[report()],
        );

        // Now we press another key, triggering the tap for the previous one.
        // The mod does not apply:
        assert_reports_eq(
            layout.reports([[true, false, false], [true, false, false]]),
            &[report().with_key(D), report().with_key(D).with_key(A)],
        );

        assert_reports_eq(
            layout.reports([[false, false, false], [true, false, false]]),
            &[report().with_key(D)],
        );
    }
}

mod preemptive_mod_hold {
    use super::*;
    use keebit::hid::LeftCtrl;
    use test_case::test_case;

    #[test_case(false)]
    #[test_case(true)]
    fn works_with_a_tap(permissive_mod_for_tap_action: bool) {
        let (_now, mut layout) = layout(permissive_mod_for_tap_action, true);

        // Pressed, the mod is activated:
        assert_reports_eq(
            layout.reports([[false, true, false], [false, false, false]]),
            &[report().with_mod(LeftCtrl)],
        );

        // Released, the mod is not active anymore:
        assert_reports_eq(
            layout.reports([[false, false, false], [false, false, false]]),
            &[report().with_key(B), report()],
        );
    }

    #[test_case(false)]
    #[test_case(true)]
    fn works_with_a_hold(permissive_mod_for_tap_action: bool) {
        let (now, mut layout) = layout(permissive_mod_for_tap_action, true);

        // Pressed, the mod is activated:
        assert_reports_eq(
            layout.reports([[false, true, false], [false, false, false]]),
            &[report().with_mod(LeftCtrl)],
        );

        now.set(1000);

        // Timed out, the mod is still active, as normal:
        assert_reports_eq(
            layout.reports([[false, true, false], [false, false, false]]),
            &[report().with_mod(LeftCtrl)],
        );
    }

    #[test_case(false)]
    #[test_case(true)]
    fn works_with_other_key_pressed(permissive_mod_for_tap_action: bool) {
        let (_now, mut layout) = layout(permissive_mod_for_tap_action, true);

        // Pressed, the mod is activated:
        assert_reports_eq(
            layout.reports([[false, true, false], [false, false, false]]),
            &[report().with_mod(LeftCtrl)],
        );

        // Another key is pressed, the mod applies to this other key:
        assert_reports_eq(
            layout.reports([[true, true, false], [false, false, false]]),
            &[report().with_mod(LeftCtrl).with_key(A)],
        );
    }
}
