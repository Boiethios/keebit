use keebit::prelude::*;

struct DummyMonotonic;

impl MonotonicTimer for DummyMonotonic {
    fn now(&mut self) -> u64 {
        0
    }
}

#[test]
fn tap_holds_need_a_monotonic_timer() {
    //todo
}

#[test]
fn max_keys_per_chord_has_the_right_value() {
    const LAYOUT: [[[MaybeAction; 1]; 1]; 1] = [[[k(A)]]];

    assert!(matches!(
        Layout::builder((), &LAYOUT)
            .with_timer(DummyMonotonic)
            .with_debounce_time(25)
            .with_chords::<3, 2>([
                (&[(0, 0)], k(A)),
                (&[(0, 0), (0, 1), (0, 2)], k(A)),
                (&[(0, 0), (0, 1)], k(A)),
            ])
            .build(),
        Err(Error::InvalidMaxKeysForChord)
    ));

    assert!(matches!(
        Layout::builder((), &LAYOUT)
            .with_timer(DummyMonotonic)
            .with_debounce_time(25)
            .with_chords::<3, 4>([
                (&[(0, 0)], k(A)),
                (&[(0, 0), (0, 1), (0, 2)], k(A)),
                (&[(0, 0), (0, 1)], k(A)),
            ])
            .build(),
        Err(Error::InvalidMaxKeysForChord)
    ));

    assert!(matches!(
        Layout::builder((), &LAYOUT)
            .with_timer(DummyMonotonic)
            .with_debounce_time(25)
            .with_chords::<3, 3>([
                (&[(0, 0)], k(A)),
                (&[(0, 0), (0, 1), (0, 2)], k(A)),
                (&[(0, 0), (0, 1)], k(A)),
            ])
            .build(),
        Ok(_)
    ));

    assert!(matches!(
        Layout::builder((), &LAYOUT)
            .with_timer(DummyMonotonic)
            .with_debounce_time(25)
            .build(),
        Ok(_)
    ));
}
