//! The simplest way to use this crate.
//!
//! This can be used as an example.

mod utils;

use keebit::{hid::*, prelude::*};
use utils::{assert_reports_eq, report};

const ROW: usize = 2;
const COL: usize = 3;

fn layout() -> Layout<NoTimer, (), 0, 0, 2, ROW, COL> {
    const LAYOUT: [[[MaybeAction; 3]; 2]; 2] = [
        [[k(A), k(B), km(C, RIGHT_CTRL)], [k(D), m(LEFT_ALT), l(1)]],
        [
            [k(Slash), k(Dot), k(Comma)],
            [k(Semicolon), k(Backspace), XXXXX],
        ],
    ];

    Layout::builder((), &LAYOUT).build().unwrap()
}

#[test]
fn empty_layers() {
    // We just verify that having a nonsensical zero-layer does not crash the code:
    let mut layout = Layout::builder((), &[[[]]]).build().unwrap();

    assert_reports_eq(layout.reports([[]]), &[report()]);
}

#[test]
fn no_keys_do_not_prevent_to_report_others() {
    fn action_mac_fn(
        _reports: &mut Reports,
        state: &mut LayoutState,
        _monotonic: &mut dyn MonotonicTimer,
        _event: Event,
    ) {
        state.report.keys_mut()[5] = Insert;
    }

    const LAYOUT: [[[MaybeAction; 1]; 1]; 1] = [[[custom(action_mac_fn)]]];

    let mut layout = Layout::builder((), &LAYOUT).build().unwrap();
    let bytes = layout
        .reports([[true]])
        .next()
        .expect("a report")
        .into_bytes();

    assert_eq!(bytes, [0, 0, 0, 0, 0, 0, 0, 0x49])
}

#[test]
fn simple_keypress() {
    let mut layout = layout();

    // First state change: key pressed:
    assert_reports_eq(
        layout.reports([[false, true, false], [false, false, false]]),
        &[report().with_key(B)],
    );

    // Second state change: key released:
    assert_reports_eq(
        layout.reports([[false, false, false], [false, false, false]]),
        &[report()],
    );
}

#[test]
fn layer_change_with_release_layer_change_first() {
    let mut layout = layout();

    // Press on the layer change:
    assert_reports_eq(
        layout.reports([[false, false, false], [false, false, true]]),
        &[report()],
    );

    // It is a Dot on this layer:
    assert_reports_eq(
        layout.reports([[false, false, true], [false, false, true]]),
        &[report().with_key(Comma)],
    );

    // The layer change key is realeased, the state still stays the same:
    assert_reports_eq(
        layout.reports([[false, false, true], [false, false, false]]),
        &[report().with_key(Comma)],
    );

    // Now, let's release the key. The report is the default one, now:
    assert_reports_eq(
        layout.reports([[false, false, false], [false, false, false]]),
        &[report()],
    );

    // Let's press the same key again, but without the layer change this time.
    // It's a Ctrl+C, now:
    assert_reports_eq(
        layout.reports([[false, false, true], [false, false, false]]),
        &[report().with_mod(RightCtrl).with_key(C)],
    );

    assert_reports_eq(
        layout.reports([[false, false, false], [false, false, false]]),
        &[report()],
    );
}

#[test]
fn layer_change_with_release_layer_change_second() {
    let mut layout = layout();

    // Press on the layer change:
    assert_reports_eq(
        layout.reports([[false, false, false], [false, false, true]]),
        &[report()],
    );

    // It is a Dot on this layer:
    assert_reports_eq(
        layout.reports([[false, false, true], [false, false, true]]),
        &[report().with_key(Comma)],
    );

    // Now, let's release the key. The report is the default one, now:
    assert_reports_eq(
        layout.reports([[false, false, false], [false, false, true]]),
        &[report()],
    );

    // The layer change key is realeased, no report is generated:
    assert_reports_eq(
        layout.reports([[false, false, false], [false, false, false]]),
        &[report()],
    );

    // Let's press the same key again, but without the layer change this time.
    // It's a Ctrl+C, now:
    assert_reports_eq(
        layout.reports([[false, false, true], [false, false, false]]),
        &[report().with_mod(RightCtrl).with_key(C)],
    );

    assert_reports_eq(
        layout.reports([[false, false, false], [false, false, false]]),
        &[report()],
    );
}

#[test]
fn releasing_a_key_after_a_second_is_pressed_should_do_nothing() {
    let mut layout = layout();

    // Press key 1 (A):
    assert_reports_eq(
        layout.reports([[true, false, false], [false, false, false]]),
        &[report().with_key(A)],
    );

    // Press key 2 (B) without releasing key 1 (A). The state should hold both keys.
    // It's especially important for the gaming when the player wants to go both up and right,
    // for example. The state must hold both keys.
    assert_reports_eq(
        layout.reports([[true, true, false], [false, false, false]]),
        &[report().with_key(A).with_key(B)],
    );

    // Now, let's release key 1. The state is only key 2 (B) is pressed:
    assert_reports_eq(
        layout.reports([[false, true, false], [false, false, false]]),
        &[report().with_key(B)],
    );

    // Let's release key 2, we now get the default report:
    assert_reports_eq(
        layout.reports([[false, false, false], [false, false, false]]),
        &[report()],
    );
}

#[test]
fn the_pressed_modifier_should_apply_correctly() {
    let mut layout = layout();

    // Press key 1 (A):
    assert_reports_eq(
        layout.reports([[true, false, false], [false, false, false]]),
        &[report().with_key(A)],
    );

    // Press the modifier, the state reflects this change:
    assert_reports_eq(
        layout.reports([[true, false, false], [false, true, false]]),
        &[report().with_key(A).with_mod(LeftAlt)],
    );

    // Press key 2 (B) without releasing key 1 (A):
    assert_reports_eq(
        layout.reports([[true, true, false], [false, true, false]]),
        &[report().with_key(A).with_mod(LeftAlt).with_key(B)],
    );

    // Now, let's release key 1. The state is only key 2 (B) + mod pressed:
    assert_reports_eq(
        layout.reports([[false, true, false], [false, true, false]]),
        &[report().with_key(B).with_mod(LeftAlt)],
    );

    // Let's release key 2, we now get the default report:
    assert_reports_eq(
        layout.reports([[false, false, false], [false, false, false]]),
        &[report()],
    );
}

#[test]
fn a_weak_mod_should_not_transfer_to_another_keypress() {
    let mut layout = layout();

    // Press A:
    assert_reports_eq(
        layout.reports([[true, false, false], [false, false, false]]),
        &[report().with_key(A)],
    );

    // Press both Ctrl+C and D at once.
    // This is a special case where we must be careful not to leak the
    // weak modifier to the next key. We can do that by returning an early intermediate state
    // with the weak mod, then the final state.
    assert_reports_eq(
        layout.reports([[true, false, true], [true, false, false]]),
        &[
            report().with_key(A).with_mod(RightCtrl).with_key(C),
            report().with_key(A).with_key(C).with_key(D),
        ],
    );
}

#[test]
fn a_mod_applies_to_a_previously_pressed_key() {
    let mut layout = layout();

    // Press A:
    assert_reports_eq(
        layout.reports([[true, false, false], [false, false, false]]),
        &[report().with_key(A)],
    );

    // Press Alt:
    assert_reports_eq(
        layout.reports([[true, false, false], [false, true, false]]),
        &[report().with_key(A).with_mod(LeftAlt)],
    );
}
