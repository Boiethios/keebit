#![allow(dead_code)]

use keebit::{
    hid::Report,
    prelude::{Layout, MonotonicTimer},
};

/// Compares 2 different series of reports.
/// For the sake of simplicity, identical consecutive states are deduplicated
/// because it does not change anything from the host PoV.
#[track_caller]
pub fn assert_reports_eq(left: impl Iterator<Item = Report>, right: &[Report]) {
    let mut left: Vec<_> = left.collect();
    left.dedup();
    //assert_eq!(left, right, "\x08\x08 was expected\n");
    assert_eq!(left, right);
}

/// Default (empty) report.
pub fn report() -> Report {
    Default::default()
}

pub struct LayoutDebouncer<
    Monotonic: MonotonicTimer,
    CustomData: 'static,
    const CHORDS: usize,
    const MAX_KEYS_PER_CHORD: usize,
    const N: usize,
    const ROW: usize,
    const COL: usize,
> {
    layout: Layout<Monotonic, CustomData, CHORDS, MAX_KEYS_PER_CHORD, N, ROW, COL>,
    time: std::sync::Arc<std::cell::Cell<u64>>,
}

impl<
        Monotonic: MonotonicTimer,
        CustomData,
        const CHORDS: usize,
        const MAX_KEYS_PER_CHORD: usize,
        const N: usize,
        const ROW: usize,
        const COL: usize,
    > LayoutDebouncer<Monotonic, CustomData, CHORDS, MAX_KEYS_PER_CHORD, N, ROW, COL>
{
    pub fn new(
        layout: Layout<Monotonic, CustomData, CHORDS, MAX_KEYS_PER_CHORD, N, ROW, COL>,
        time: std::sync::Arc<std::cell::Cell<u64>>,
    ) -> Self {
        Self { layout, time }
    }

    pub fn reports(&mut self, state: [[bool; COL]; ROW]) -> impl Iterator<Item = Report> + '_ {
        let _reports: Vec<_> = self.layout.reports(state).collect();

        self.time.set(self.time.get() + 30);

        self.layout.reports(state)
    }
}
