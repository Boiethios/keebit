//! Tests the mechanism related to layers:
//! - Usage of different default layers;
//! - Usage of the below field in the state.

mod utils;

use keebit::{hid::*, prelude::*};
use utils::{assert_reports_eq, report};

const ROW: usize = 1;
const COL: usize = 3;

fn layout() -> Layout<NoTimer, (), 0, 0, 3, ROW, COL> {
    const LAYOUT: [[[MaybeAction; 3]; 1]; 3] = [
        [[k(A), custom(change_below_algo), l(2)]],
        [[k(X), k(Y), ml(RIGHT_ALT, 2)]],
        [[_____, _____, k(Comma)]],
    ];

    Layout::builder((), &LAYOUT)
        .with_below(|layer| match layer {
            2 => Some(0),
            _ => None,
        })
        .build()
        .unwrap()
}

fn change_below_algo(
    _reports: &mut Reports,
    state: &mut LayoutState,
    _monotonic: &mut dyn MonotonicTimer,
    event: Event,
) {
    if event.is_pressed() {
        state.below = None;
        state.active_layers.change_base(1);
    }
}

#[test]
fn below_logic_allows_to_skip_one_layer() {
    let mut layout = layout();

    // Move to layer 2:
    assert_reports_eq(layout.reports([[false, false, true]]), &[report()]);

    // Press a key:
    assert_reports_eq(
        layout.reports([[true, false, true]]),
        &[report().with_key(A)],
    );

    // Release the layer change:
    assert_reports_eq(
        layout.reports([[true, false, false]]),
        &[report().with_key(A)],
    );

    // Release the key:
    assert_reports_eq(layout.reports([[false, false, false]]), &[report()]);
}

#[test]
fn mofify_the_below_logic_with_a_custom_action() {
    let mut layout = layout();

    // Key A is pressed:
    assert_reports_eq(
        layout.reports([[true, false, false]]),
        &[report().with_key(A)],
    );

    // Key A is released:
    assert_reports_eq(layout.reports([[false, false, false]]), &[report()]);

    // Press the custom key:
    assert_reports_eq(layout.reports([[false, true, false]]), &[report()]);

    // Release the custom key:
    assert_reports_eq(layout.reports([[false, false, false]]), &[report()]);

    assert_eq!(layout.state().active_layers.active(), 1);

    // The key previously being A is now X:
    assert_reports_eq(
        layout.reports([[true, false, false]]),
        &[report().with_key(X)],
    );

    assert_reports_eq(layout.reports([[false, false, false]]), &[report()]);

    // When we go to the layer 2:
    assert_reports_eq(
        layout.reports([[false, false, true]]),
        &[report().with_mod(RightAlt)],
    );

    // The new transparency algo makes it's X again:
    assert_reports_eq(
        layout.reports([[true, false, true]]),
        &[report().with_mod(RightAlt).with_key(X)],
    );

    assert_reports_eq(
        layout.reports([[false, false, true]]),
        &[report().with_mod(RightAlt)],
    );

    assert_reports_eq(layout.reports([[false, false, false]]), &[report()]);
}
