# ![Keebit](keebit-logo.svg 'Keebit')

[![Version](https://shields.io/crates/v/keebit.svg)](https://crates.io/crates/keebit)
[![Documentation](https://docs.rs/keebit/badge.svg)](https://docs.rs/keebit)
[![License](https://shields.io/gitlab/license/Boiethios/keebit)](https://gitlab.com/Boiethios/LICENSE)

Build rich keyboard layouts with ease.

## High-level Overview

Conceptually, Keebit is really simple: it behaves like a function which takes
keyboard states (an array of `bool`s), and returns new HID reports to be send
to the host.

The library does _only_ that, and is not intended to replace firmware frameworks
like QMK. To put it otherly, the hardware code (matrix scanning, LEDs, etc.)
is not included in this library, which remains hardware-agnostic.

### Goals

- A feature complete library. Everything that is typing related should be handled, so that even
  the most hardcore power users find it useful.
- No user-facing macros. The Rust typesystem should be enough.

### Non-goals

- No hardware code.
- No things non-typing related (RGB keys lighting, encoders, etc.) unless it is proven it is needed
  because the concerns cannot be clearly separated.

## License

The code is under the LGPL license, which means that it can be used in
commercial, closed-source projects, but every change applied to this library
_must_ be open-sourced under that same LGPL licensing.

## Prior Art & Ideas

### Tap/Hold

- ZMK's behavior: https://zmk.dev/docs/behaviors/hold-tap
    - Not only 'hold-preferred' flavor, but some others ('tap-preferred', 'tap-unless-interrupted', and 'balanced')
    - 

## TODO

- Make the chords const as well, like with the layers
- Use a bunch of compile-time const, for example, to allow changing some values, for example, the size of a HID report:
  - The exact mechanism is to be determined, but it could be a variable env to change.
- Tests/to investigate:
  - One-Shot:
    - Add a test to see if a OS action in a tap-hold one works.
    - How to cancel an OS action? Is it "legal" to put an action on the place of the OS one in a different layer?
    - What if 2 OSes are coming one after the other?
- Generate a HTML visualy showing the layout from keebit
- Make the type in the layers generic? Something like `Borrow<MaybeAction>` or `AsRef<MaybeAction>`?
- Re-enable the reserved byte, as it is not supported since the last rewrite.
