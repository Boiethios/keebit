#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct Coord {
    row: u8,
    col: u8,
}

impl Coord {
    pub fn new(row: u8, col: u8) -> Self {
        Self { row, col }
    }

    //pub(crate) fn iter<const ROW: usize, const COL: usize>() -> CoordIter<ROW, COL> {
    //    Default::default()
    //}

    pub fn row(self) -> usize {
        self.row.into()
    }

    pub fn col(self) -> usize {
        self.col.into()
    }
}

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq)]
pub(crate) struct CoordIter<const ROW: usize, const COL: usize> {
    coo: Coord,
}

impl<const ROW: usize, const COL: usize> Iterator for CoordIter<ROW, COL> {
    type Item = Coord;

    fn next(&mut self) -> Option<Self::Item> {
        (self.coo.row != ROW as u8).then(|| {
            let result = self.coo;
            self.coo.col += 1;
            if self.coo.col == COL as u8 {
                self.coo.row += 1;
                self.coo.col = 0;
            }
            result
        })
    }
}
