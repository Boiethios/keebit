/// Allows the `StateHandler` and the `Layout` to spawn events.
pub trait MonotonicTimer {
    /// Returns a number representing an instant.
    fn now(&mut self) -> u64;
}

// A dummy monotonic implementation to be used as the monotonic type when the
// user does not use this feature
impl MonotonicTimer for crate::layout::builder::NoTimer {
    fn now(&mut self) -> u64 {
        0
    }
}
