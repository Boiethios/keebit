use core::fmt;

/// Creates a new heapless vector.
#[macro_export]
macro_rules! vec {
    [ $( $item:expr ),* $(,)? ] => {{
        #[allow(unused_mut)]
        let mut v = ::heapless::Vec::new();
        $( v.push($item).unwrap(); )*
        v
    }};
}
pub use crate::vec;
pub use ::heapless::Vec;

/// Displays a message only during tests and with the `tracing` feature enabled.
#[macro_export]
macro_rules! trace {
    [ $( $item:expr ),* $(,)? ] => {{
        #[cfg(feature = "tracing")]
        {
            let message = format!( $( $item ),* );
            eprintln!("{message}");
        }
    }};
}

/// 0   0   0
/// 0   1   0
/// 1   0   1
/// 1   1   0
///
/// P ∧ ¬Q
pub const fn nimply(p: u8, q: u8) -> u8 {
    p & !q
}

pub fn try_collect<I, const N: usize>(it: I) -> Result<Vec<I::Item, N>, ()>
where
    I: IntoIterator,
{
    let mut result = Vec::new();

    for item in it {
        result.push(item).map_err(|_| ())?;
    }

    Ok(result)
}

/// Wrapper to optimize the memory footprint of `Option<u8>` by using the max value as `None`.
#[derive(Clone, Copy, PartialEq, Eq)]
pub struct MaybeU8(u8);

impl MaybeU8 {
    pub const fn from_u8(value: u8) -> Self {
        Self(value)
    }

    pub const fn get(self) -> Option<u8> {
        // Cannot use `self` directly, because it is bugged on nightly:
        match self.0 {
            u8::MAX => None,
            u => Some(u),
        }
    }

    pub fn set(&mut self, value: Option<u8>) {
        *self = match value {
            Some(u) => Self(u),
            None => Self::NONE,
        }
    }

    pub const NONE: Self = MaybeU8(u8::MAX);
}

impl From<u8> for MaybeU8 {
    fn from(value: u8) -> Self {
        Self(value)
    }
}

impl Default for MaybeU8 {
    fn default() -> Self {
        Self::NONE
    }
}

impl fmt::Debug for MaybeU8 {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.get() {
            Some(u) => write!(f, "U8({u})"),
            None => write!(f, "NONE"),
        }
    }
}

#[test]
fn default_maybe_u8_is_none() {
    assert_eq!(MaybeU8::default().get(), None);
}

#[cfg(feature = "reserved")]
pub fn activate_reserved_bits<CustomData>(
    state: &mut crate::layout::LayoutState<CustomData>,
    bits: u8,
) {
    state.strong_reserved_byte.0 |= bits
}

#[cfg(not(feature = "reserved"))]
pub fn activate_reserved_bits<CustomData>(
    _state: &mut crate::layout::LayoutState<CustomData>,
    _bits: u8,
) {
}

#[cfg(feature = "reserved")]
pub fn deactivate_reserved_bits<CustomData>(
    state: &mut crate::layout::LayoutState<CustomData>,
    bits: u8,
) {
    state.strong_reserved_byte.0 = nimply(state.strong_reserved_byte.0, bits)
}

#[cfg(not(feature = "reserved"))]
pub fn deactivate_reserved_bits<CustomData>(
    _state: &mut crate::layout::LayoutState<CustomData>,
    _bits: u8,
) {
}

#[cfg(test)]
pub trait SizeOf {
    fn size_of(arch_in_bytes: usize) -> usize;
}
