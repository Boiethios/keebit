/// All of the layer actions.
pub mod actions;
pub(crate) mod actions_mut;

pub(crate) mod builder;
mod timeout;

mod active_layers;
pub use active_layers::ActiveLayers;

use crate::{
    chords::Chords,
    coord::Coord,
    hid::{ModifiersByte, Report},
    prelude::MonotonicTimer,
    state::{Event, EventKind, EventPlace, StateHandler},
    util::{try_collect, vec, MaybeU8, Vec},
};
use actions::{Action, MaybeAction};
use actions_mut::ActionMutData;
use core::ops::Not as _;
use either::Either;
use timeout::Timeout;

/// A growable list of records.
pub type Reports = Vec<Report, 32>;

/// A complete layout, with several layers and an internal state.
#[derive(Debug)]
pub struct Layout<
    Monotonic: MonotonicTimer,
    CustomData: 'static,
    const N_CHORDS: usize,
    const MAX_KEYS_PER_CHORD: usize,
    const N_LAYERS: usize,
    const ROW: usize,
    const COL: usize,
> {
    /// The hardware state handler, that yields events.
    state_handler: StateHandler<ROW, COL>,
    /// Holds the user defined layout: layers, chords, etc. But not their state.
    read_only_layers_info: &'static [[[MaybeAction<CustomData>; COL]; ROW]; N_LAYERS],
    /// Holds the mutable part for the user defined layout.
    //mutable_layers_info: [[[ActionMutData; COL]; ROW]; N_LAYERS],
    mutable_layers_info: [[ActionMutData; COL]; ROW],
    /// Special keys timeout management.
    monotonic: Monotonic,
    /// The time a state must remain the same before events are fired up.
    debounce_time: u64,
    /// Record the layer the keys are pressed in, so that the release & timeout events can be sent
    /// to the appropriate keys.
    pressed_keys: [[MaybeU8; COL]; ROW],
    /// A series of pressed keys that may be a part of a chord.
    /// The events are held aside because they are cancelled in case of a chord event.
    chords: Chords<CustomData, N_CHORDS, MAX_KEYS_PER_CHORD>,
    /// The internal state of the layout, including the various configuration.
    layout_state: LayoutState<CustomData>,
}

const MAX_LAYERS_ACTIVE: usize = 4; //TODO take this value from an env variable

/// The state of the whole layout (for example, hold the current layer).
#[derive(Debug)]
pub struct LayoutState<CustomData = ()> {
    /// Active layer index.
    pub active_layers: ActiveLayers<MAX_LAYERS_ACTIVE>,
    /// Currently active keys.
    /// The mods inside the report are weak mods, so each time a new key is pushed,
    /// the mod field is replaced.
    pub report: Report,
    /// Currently active (strong) modifiers.
    pub strong_modifiers: ModifiersByte,
    /// Currently active (strong) reserved byte.
    #[cfg(feature = "reserved")]
    pub strong_reserved_byte: crate::prelude::ReservedByte,
    /// The current timeout for a tap/hold action.
    current_timeout: Timeout,
    /// The currently active one-shot action (layer + coordinate).
    current_one_shot_action: Option<(u8, EventPlace)>,

    /// The logic to express which layer is behind which.
    pub below: Option<fn(u8) -> Option<u8>>,
    /// This is the default time before an action is considered a hold.
    pub default_timeout: u64,
    /// # Explanation
    ///
    /// Given this timeline:
    ///
    /// ```none
    ///   |   modifier   |
    ///           |   tap   |
    /// ----------------------->
    /// ```
    ///
    /// Since the tap is determinated to be a tap only after its release, the mod is not
    /// supposed to apply to the key. This behavior can be surprising.
    ///
    /// - true: The mod applies to the tap even if it is released before the tap is,
    /// - false: The mod does not apply to the tap.
    ///
    /// # Recommandation
    ///
    /// Note that this behavior can be tweaked on a on a case-by-case basis.
    /// What is recommanded is:
    /// - Use true for the regular modifiers,
    /// - Use false for the homerow modifiers.
    ///
    /// # Default value
    ///
    /// ```
    /// # use keebit::prelude::*;
    /// assert!(true == Layout::builder((), &[[[XXXXX]]]).build().unwrap().state().permissive_mod_for_tap_action)
    /// ```
    pub permissive_mod_for_tap_action: bool,
    /// # Explanation
    ///
    /// Applies when a hold action is a modifier:
    ///
    /// - true: The mod is applied immediately, as soon as the key is pressed.
    ///         If the key, in the future is determinated to be a tap, the mod is cancelled.
    ///         It is useful when you want to CTRL + click, for example,
    ///         without waiting for the hold to fire.
    /// - false: The mod is applied only when it is determinated to be a hold.
    ///
    /// # Default value
    ///
    /// ```
    /// # use keebit::prelude::*;
    /// assert!(true == Layout::builder((), &[[[XXXXX]]]).build().unwrap().state().preemptive_mod_hold)
    /// ```
    pub preemptive_mod_hold: bool,
    /// Some user data added to the state, so that it can be modified when a custom action is called.
    pub custom_data: CustomData,
}

fn action_wrapper<'a, CustomData, const ROW: usize, const COL: usize>(
    layout: &'a [[[MaybeAction<CustomData>; COL]; ROW]],
    index: u8,
    coo: Coord,
) -> &'a MaybeAction<CustomData> {
    &(match layout.get(usize::from(index)) {
        Some(layer) => layer,
        None => layout.last().unwrap(),
    })[coo.row()][coo.col()]
}

/// Gets what the active layer is for a given coordinate, *i.e.* handles the transparency.
fn layer_with_transparency<CustomData, const ROW: usize, const COL: usize>(
    layout: &[[[MaybeAction<CustomData>; COL]; ROW]],
    below: Option<fn(u8) -> Option<u8>>,
    active: u8,
    coo: Coord,
) -> Option<u8> {
    let mut layer_index = active;
    while let MaybeAction::Transparent = action_wrapper(layout, layer_index, coo) {
        layer_index = (below.unwrap_or(|u| u.checked_sub(1)))(layer_index)?;
    }
    Some(layer_index)
}

fn action_data<
    'a,
    CustomData,
    const ROW: usize,
    const COL: usize,
    const CHORDS: usize,
    const MAX_KEYS_PER_CHORD: usize,
>(
    place: EventPlace,
    chords: &'a mut Chords<CustomData, CHORDS, MAX_KEYS_PER_CHORD>,
    layers: &'a [[[MaybeAction<CustomData>; COL]; ROW]],
    layers_data_mut: &'a mut [[ActionMutData; COL]; ROW],
    pressed_keys: &[[MaybeU8; COL]; ROW],
    below: Option<fn(u8) -> Option<u8>>,
    active: u8,
) -> Option<(&'a Action<CustomData>, &'a mut ActionMutData)> {
    match place {
        EventPlace::Chord(index) => chords.action_mut_at(index),
        EventPlace::Key(coo) => {
            let layer = pressed_keys[coo.row()][coo.col()]
                .get()
                .or_else(|| layer_with_transparency(layers, below, active, coo))?;
            let index = (layers.len() - 1).min(usize::from(layer));

            layers[index][coo.row()][coo.col()]
                .action()
                .map(|action| (action, &mut layers_data_mut[coo.row()][coo.col()]))
        }
    }
}

impl<
        Monotonic: MonotonicTimer,
        CustomData,
        const CHORDS: usize,
        const MAX_KEYS_PER_CHORD: usize,
        const N: usize,
        const ROW: usize,
        const COL: usize,
    > Layout<Monotonic, CustomData, CHORDS, MAX_KEYS_PER_CHORD, N, ROW, COL>
{
    /// Process an event and return an iterator which yields all produced reports,
    /// which are to be sent to the host.
    pub fn reports(&mut self, new_state: [[bool; COL]; ROW]) -> impl Iterator<Item = Report> + '_ {
        core::iter::from_coroutine(move || {
            let events = self.state_handler.events(
                new_state,
                crate::timer::MonotonicTimer::now(&mut self.monotonic),
                self.debounce_time,
            );

            // If chords are active, we need to collect the events to verify they match a chord:
            let events = if self.chords.has_some() {
                // Arbitrary size of collected events (20) because we cannot do operations on const params:
                let result: Result<Vec<_, 20>, ()> =
                    handle_chording_events(events, &mut self.chords);
                Either::Right(match result {
                    Ok(e) => e.into_iter(),
                    Err(()) => {
                        yield vec![Report::rollover()];
                        return;
                    }
                })
            } else {
                Either::Left(events)
            };

            // Check that the current tap/hold action (if any) has timed out:
            if let Some(place) = self
                .layout_state
                .current_timeout
                .elapsed(&mut self.monotonic)
            {
                if let Some((Action::TapOrHold(action), data_mut)) = action_data(
                    place,
                    &mut self.chords,
                    self.read_only_layers_info,
                    &mut self.mutable_layers_info,
                    &self.pressed_keys,
                    self.layout_state.below,
                    self.layout_state.active_layers.active(),
                ) {
                    yield action.timed_out(
                        data_mut,
                        &mut self.layout_state,
                        &mut self.monotonic,
                        place,
                    );
                }
            }

            for event in events {
                // Register a key press in the pressed keys table:
                if let Event {
                    place: EventPlace::Key(coo),
                    kind: EventKind::Pressed,
                } = event
                {
                    self.pressed_keys[coo.row()][coo.col()].set(layer_with_transparency(
                        self.read_only_layers_info,
                        self.layout_state.below,
                        self.layout_state.active_layers.active(),
                        coo,
                    ));
                }

                // Modify the tap/hold action if another key is pressed:
                if event.kind == EventKind::Pressed {
                    if let Some(place) = self.layout_state.current_timeout.take() {
                        if let Some((Action::TapOrHold(action), data_mut)) = action_data(
                            place,
                            &mut self.chords,
                            self.read_only_layers_info,
                            &mut self.mutable_layers_info,
                            &self.pressed_keys,
                            self.layout_state.below,
                            self.layout_state.active_layers.active(),
                        ) {
                            yield action.apply_other_keypress(
                                data_mut,
                                place,
                                &mut self.layout_state,
                                &mut self.monotonic,
                            );
                        }
                    }
                }

                //TODO broadcast event to custom pre-action function.

                // Action:
                if let Some((action, data_mut)) = action_data(
                    event.place,
                    &mut self.chords,
                    self.read_only_layers_info,
                    &mut self.mutable_layers_info,
                    &self.pressed_keys,
                    self.layout_state.below,
                    self.layout_state.active_layers.active(),
                ) {
                    yield action.handle_event(
                        data_mut,
                        &mut self.layout_state,
                        &mut self.monotonic,
                        event,
                    );
                }

                // Handle the oneshot action:
                if let Some((layer, place)) = self
                    .layout_state
                    .current_one_shot_action
                    .filter(|(_, place)| &event.place != place)
                {
                    if let Some((action, data_mut)) = action_data(
                        place,
                        &mut self.chords,
                        self.read_only_layers_info,
                        &mut self.mutable_layers_info,
                        &self.pressed_keys,
                        None, /* It won't be used */
                        layer,
                    ) {
                        action.cleanup(data_mut, &mut self.layout_state);
                    }
                    self.layout_state.current_one_shot_action = None;
                }

                //TODO broadcast event to custom post-action function.

                // Remove a released key from the pressed keys table:
                if let Event {
                    place: EventPlace::Key(coo),
                    kind: EventKind::Released,
                } = event
                {
                    self.pressed_keys[coo.row()][coo.col()].set(None);
                }
            }

            {
                #[cfg(not(feature = "reserved"))]
                let report = self
                    .layout_state
                    .report
                    .with_mod(self.layout_state.strong_modifiers);
                #[cfg(feature = "reserved")]
                let report = self
                    .layout_state
                    .report
                    .with_mod(self.layout_state.strong_modifiers)
                    .with_activated_reserved_bits(self.layout_state.strong_reserved_byte);
                yield vec![report];
            }
        })
        .flatten()
    }

    /// The full layout state, including the configuration. See [`LayoutState`].
    pub fn state(&mut self) -> &LayoutState<CustomData> {
        &mut self.layout_state
    }
}

/// If we cannot return events, we return an error. In that case,
/// the caller returns a rollover report and ignore all the events.
///
/// # How it works
///
/// - A key is pressed:
///     - If the key is used in any “building” chord (ie, there is a “free space” in the chord),
///       put it inside the `chords_keys` field.
///         - If the chord is incomplete, do nothing.
///         - If the chord is complete, mark it as pressed. There shall be a timeout notion here.
///     - If the key is used in no “building” chord (ie, the pressed keys match no existing chord),
///       empty the `chords_keys` in order, and treat them normally.
/// - A key is released:
///     - If there is no chord pressed, empty `chords_keys` and
///       treat the other pressed keys before the currently released one.
///       For example, if we have a AUI chord, and events A-pressed, U-pressed, U-released, we must write “au”
///     - If there is a chord pressed, handle it as any other key.
/// - Timeout:
///     - If that chord has a timeout, empty the `chords_keys`.
fn handle_chording_events<
    CustomData,
    const SIZE: usize,
    const CHORDS: usize,
    const MAX_KEYS_PER_CHORD: usize,
>(
    events: impl Iterator<Item = Event>,
    chords: &mut crate::chords::Chords<CustomData, CHORDS, MAX_KEYS_PER_CHORD>,
) -> Result<Vec<Event, SIZE>, ()> {
    let mut events: Vec<_, SIZE> = try_collect(events)?;

    if let Some(chord_index) = chords.matches(&events) {
        chords.activate(chord_index);
        events
            .iter_mut()
            .find(|event| event.kind == EventKind::Pressed)
            .expect("a press event should exist because we got a chord press")
            .place = EventPlace::Chord(chord_index);

        try_collect(
            events
                .into_iter()
                .filter(event_is_not_a_regular_press)
                .filter_map(|event| chords.released(event)),
        )
    } else {
        try_collect(
            events
                .into_iter()
                .filter_map(|event| chords.released(event)),
        )
    }
}

fn event_is_not_a_regular_press(event: &Event) -> bool {
    matches!(
        event,
        Event {
            kind: EventKind::Pressed,
            place: EventPlace::Key(_)
        }
    )
    .not()
}
