#![doc = include_str!("../README.md")]
#![cfg_attr(all(not(tests), not(feature = "tracing")), no_std)]
#![feature(
    coroutines,
    iter_from_coroutine,
    const_trait_impl,
    effects,
    array_try_map
)]
#![forbid(unsafe_code)]
#![deny(missing_debug_implementations, missing_docs)]

/// The Human Interface Devices report types.
pub mod hid;
/// The layout actions, builder, timeout, various code for international layouts, etc.
pub mod layout;

mod chords;
mod coord;
mod error;
mod state;
mod timer;
mod util;

/// `use keebit::prelude::*;` to import what is needed to write a full layout.
pub mod prelude {
    /// Left control key.
    pub const LEFT_CTRL: u8 = crate::hid::modifiers_consts::LeftCtrl.0;
    /// Left shift key.
    pub const LEFT_SHIFT: u8 = crate::hid::modifiers_consts::LeftShift.0;
    /// Left alt key.
    pub const LEFT_ALT: u8 = crate::hid::modifiers_consts::LeftAlt.0;
    /// Left “gui” or “meta” or “super” or “windows” key.
    pub const LEFT_GUI: u8 = crate::hid::modifiers_consts::LeftGui.0;
    /// Right control key.
    pub const RIGHT_CTRL: u8 = crate::hid::modifiers_consts::RightCtrl.0;
    /// Right shift key.
    pub const RIGHT_SHIFT: u8 = crate::hid::modifiers_consts::RightShift.0;
    /// Right alt key.
    pub const RIGHT_ALT: u8 = crate::hid::modifiers_consts::RightAlt.0;
    /// Right “gui” or “meta” or “super” or “windows” key.
    pub const RIGHT_GUI: u8 = crate::hid::modifiers_consts::RightGui.0;

    pub use self::{
        LEFT_ALT as LEFT_OPTION, LEFT_GUI as LEFT_COMMAND, RIGHT_ALT as RIGHT_OPTION,
        RIGHT_GUI as RIGHT_COMMAND,
    };

    pub use crate::{
        error::Error,
        hid::{KeyCode::*, Report},
        layout::{
            actions::{
                custom, hold, k, km, l, l1, m, m1, macr, ml, ml1, sw_l, sw_m, sw_ml, Action,
                MaybeAction,
            },
            builder::NoTimer,
            Layout, LayoutState, Reports,
        },
        state::{Event, EventKind, EventPlace},
        timer::MonotonicTimer,
        util::{vec, Vec},
    };

    pub use MaybeAction::{Nothing as XXXXX, Transparent as _____};

    #[cfg(feature = "reserved")]
    pub use crate::hid::ReservedByte;
}
