use crate::layout::actions_mut::ActionMutData;

use {
    crate::{
        coord::Coord,
        layout::actions::Action,
        state::{Event, EventKind, EventPlace},
        util::Vec,
    },
    core::ops::Not,
};

#[derive(Debug)]
pub(crate) struct Chords<CustomData, const N_CHORDS: usize, const MAX_KEYS_PER_CHORD: usize> {
    read_only_chords_data: [Chord<CustomData, MAX_KEYS_PER_CHORD>; N_CHORDS],
    mutable_chords_info: [ActionMutData; N_CHORDS],
}

#[derive(Debug)]
pub struct Chord<CustomData, const MAX_KEYS_PER_CHORD: usize> {
    pub(crate) keys: Vec<Key, MAX_KEYS_PER_CHORD>,
    pub(crate) action: Action<CustomData>,
    pub(crate) active: bool,
}

#[derive(Debug)]
pub struct Key {
    pub(crate) coo: Coord,
    pub(crate) pressed: bool,
}

impl<CustomData, const N_CHORDS: usize, const MAX_KEYS_PER_CHORD: usize>
    Chords<CustomData, N_CHORDS, MAX_KEYS_PER_CHORD>
{
    pub(crate) fn new(chords: [Chord<CustomData, MAX_KEYS_PER_CHORD>; N_CHORDS]) -> Self {
        Self {
            read_only_chords_data: chords,
            mutable_chords_info: [Default::default(); N_CHORDS],
        }
    }

    pub(crate) fn has_some(&self) -> bool {
        self.read_only_chords_data.is_empty().not()
    }

    /// Returns the index of the matching chord, if any.
    pub(crate) fn matches(&mut self, events: &[Event]) -> Option<usize> {
        self.read_only_chords_data.iter_mut().position(|chord| {
            events
                .iter()
                .filter_map(|e| match e {
                    Event {
                        kind: EventKind::Pressed,
                        place: EventPlace::Key(coo),
                    } => Some(coo),
                    _ => None,
                })
                .eq(chord.keys.iter().map(|k| &k.coo))
        })
    }

    pub(crate) fn action_mut_at(
        &mut self,
        index: usize,
    ) -> Option<(&Action<CustomData>, &mut ActionMutData)> {
        match (
            self.read_only_chords_data.get(index),
            self.mutable_chords_info.get_mut(index),
        ) {
            (Some(chord), Some(chord_mut)) => Some((&chord.action, chord_mut)),
            _ => None,
        }
    }

    /// Sets a chord as activated at `index`.
    pub(crate) fn activate(&mut self, index: usize) {
        if let Some(chord) = self.read_only_chords_data.get_mut(index) {
            chord.active = true;
            for key in &mut chord.keys {
                key.pressed = true;
            }
        }
    }

    /// Returns a series of chord release events based on the key event passed in.
    /// 3 outcomes:
    /// - Nothing special happens, returns `Some(the_original_event)`;
    /// - The event has been filtered out, returns `None`;
    /// - the event has been replaced with a chording event, returns `Some(the_new_chording_event)`;
    pub(crate) fn released(&mut self, event: Event) -> Option<Event> {
        match event {
            Event {
                kind: EventKind::Released,
                place: EventPlace::Key(coo),
            } => {
                let mut result = Some(event);

                for (chord_index, chord) in self.read_only_chords_data.iter_mut().enumerate() {
                    // If the chord were active and there is no more key pressed in it,
                    // the chord becomes inactive
                    if chord.active
                        && chord
                            .keys
                            .iter_mut()
                            .fold(false, |acc, key| {
                                if key.pressed && key.coo == coo {
                                    if matches!(
                                        result,
                                        Some(Event {
                                            place: EventPlace::Key(_),
                                            ..
                                        })
                                    ) {
                                        result = None;
                                    }
                                    key.pressed = false;
                                }
                                acc || key.pressed
                            })
                            .not()
                    {
                        chord.active = false;
                        result = Some(Event {
                            kind: EventKind::Released,
                            place: EventPlace::Chord(chord_index),
                        });
                    }
                }

                result
            }
            _ => Some(event),
        }
    }
}
