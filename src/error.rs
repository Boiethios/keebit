/// All the errors we can get at initialization.
#[derive(Debug)]
pub enum Error {
    /// There are either many key coordinates composing a chord,
    /// or the limit is too big. For example, ther key-per-chords limit is 4,
    /// but there are maximum 3 keys in the chords.
    InvalidMaxKeysForChord,
}
