use crate::hid::ModifiersByte;

#[derive(Debug, Default, Clone, Copy)]
pub struct ActionMutData([u8; 2]);

impl ActionMutData {
    /// The permissive mod modifiers.
    ///
    /// Holds the modifiers activated during the keypress, because if the typist is quick,
    /// the mod key can be released before we know it's a tap.
    pub(crate) fn set_modifiers(&mut self, value: ModifiersByte) {
        self.0[0] = value.0;
    }

    pub(crate) fn modifiers(&mut self) -> ModifiersByte {
        ModifiersByte(self.0[0])
    }

    pub(crate) fn set_is_hold(&mut self, value: bool) {
        self.0[1] = value.into();
    }

    pub(crate) fn is_hold(&self) -> bool {
        self.0[1] != 0
    }
}
