use crate::{
    hid::ModifiersByte,
    layout::{
        actions::{HoldAction, TapAction},
        actions_mut::ActionMutData,
        LayoutState, Reports,
    },
    state::{Event, EventKind, EventPlace},
    timer::MonotonicTimer,
};
use core::{fmt, ops::Not as _};

/// Discriminates between tap & hold.
///
/// # Logic
///
/// ```mermaid
/// flowchart TD
///     Nothing[Nothing\n<small>timeout: empty or other\ntype: no</small>]
///     Pressed[Pressed\n<small>timeout: self\ntype: no\naction: nothing</small>]
///     Released1[Released\n<small>timeout: empty\ntype: tap\naction: set then reset</small>]
///     Timedout1[Timedout\n<small>timeout: empty\ntype: hold\naction: set</small>]
///     Released2[Released\n<small>timeout: empty or other\ntype: hold\naction: reset</small>]
///     ChooseType{Is my hold type\na tap when another\nkey is pressed?}
///     Tap[Pressed\n<small>timeout: empty or other\ntype: tap\naction: set</small>]
///     Hold[Pressed\n<small>timeout: empty or other\ntype: hold\naction: set</small>]
///     Released3[Released\n<small>timeout: empty or other\ntype: tap\naction: reset</small>]
///     Released4[Released\n<small>timeout: empty or other\ntype: hold\naction: reset</small>]
///
///     Nothing -->|Pressed| Pressed
///     Pressed -->|Released| Released1
///     Pressed -->|Timeout| Timedout1
///     Timedout1 --> |Released| Released2
///     Pressed -->|Other Key Pressed| ChooseType
///     ChooseType -->|Yes| Tap
///     ChooseType -->|No| Hold
///     Tap --> |Released| Released3
///     Hold --> |Released| Released4
/// ```
pub struct TapHoldAction<CustomData> {
    pub(crate) tap: TapAction<CustomData>,
    pub(crate) hold: HoldAction<CustomData>,
    pub(crate) permissive_mod_for_tap_action: Option<bool>,
    pub(crate) preemptive_mod_hold: Option<bool>,
}

impl<CustomData> TapHoldAction<CustomData> {
    /// When an event unrelated to this action happens, it's broadcasted to the tap/hold actions.
    pub fn apply_other_keypress(
        &self,
        data_mut: &mut ActionMutData,
        self_place: EventPlace,
        state: &mut LayoutState<CustomData>,
        monotonic: &mut impl MonotonicTimer,
    ) -> Reports {
        let mut reports = Default::default();
        data_mut.set_is_hold(matches!(self.hold, HoldAction::KeyPress(_)).not());

        let event = Event {
            kind: EventKind::Pressed,
            place: self_place,
        };
        if data_mut.is_hold() {
            self.hold
                .act(&mut reports, data_mut, state, monotonic, event)
        } else {
            let saved_state_modifiers = state.strong_modifiers;
            state.strong_modifiers += data_mut.modifiers();
            self.tap
                .act(&mut reports, data_mut, state, monotonic, event);
            state.strong_modifiers = saved_state_modifiers;
        }

        reports
    }

    /// Adds the reports to be sent to the host to `reports`.
    pub fn act(
        &self,
        reports: &mut Reports,
        data_mut: &mut ActionMutData,
        state: &mut LayoutState<CustomData>,
        monotonic: &mut impl MonotonicTimer,
        event: Event,
    ) {
        crate::trace!("TapHoldAction handles event {event:?}");

        match event.kind {
            EventKind::Pressed => {
                if self
                    .permissive_mod_for_tap_action
                    .unwrap_or(state.permissive_mod_for_tap_action)
                {
                    data_mut.set_modifiers(state.strong_modifiers)
                }
                if let Some(modifiers) = self.maybe_preemptive_mod(state) {
                    state.strong_modifiers += modifiers;
                }
                state
                    .current_timeout
                    .set(monotonic, event.place, state.default_timeout);
            }
            // When the key is released after being pressed, without a timeout nor another
            // key being pressed, it's a special case, because one must send a record for the key
            // and clean it up at once (see diagram).
            EventKind::Released => {
                if let Some(modifiers) = self.maybe_preemptive_mod(state) {
                    state.strong_modifiers -= modifiers;
                }
                match state.current_timeout.ongoing() {
                    Some(place) if place == event.place => {
                        state.current_timeout.clear();

                        let saved_state_modifiers = state.strong_modifiers;
                        state.strong_modifiers += data_mut.modifiers();
                        self.tap.act(
                            reports,
                            data_mut,
                            state,
                            monotonic,
                            Event {
                                kind: EventKind::Pressed,
                                place,
                            },
                        );
                        state.strong_modifiers = saved_state_modifiers;

                        self.tap.act(reports, data_mut, state, monotonic, event)
                    }
                    _ => {
                        if data_mut.is_hold() {
                            self.hold.act(reports, data_mut, state, monotonic, event)
                        } else {
                            self.tap.act(reports, data_mut, state, monotonic, event)
                        }
                    }
                }
            }
        }
    }

    pub(crate) fn timed_out(
        &self,
        data_mut: &mut ActionMutData,
        state: &mut LayoutState<CustomData>,
        monotonic: &mut impl MonotonicTimer,
        place: EventPlace,
    ) -> Reports {
        crate::trace!("TapHoldAction timed out");

        let mut reports = Default::default();

        data_mut.set_is_hold(true);
        // If the timeout fires, it is a hold:
        self.hold.act(
            &mut reports,
            data_mut,
            state,
            monotonic,
            Event {
                kind: EventKind::Pressed,
                place,
            },
        );

        reports
    }

    pub(crate) fn maybe_preemptive_mod(
        &self,
        config: &LayoutState<CustomData>,
    ) -> Option<ModifiersByte> {
        if self
            .preemptive_mod_hold
            .unwrap_or(config.preemptive_mod_hold)
        {
            match &self.hold {
                HoldAction::KeyPress(_) => None,
                HoldAction::ModLayer(action) if action.modifiers.is_none() => None,
                HoldAction::ModLayer(action) => Some(action.modifiers),
                HoldAction::Macro(_custom) => None,
                HoldAction::Custom(_custom) => None,
            }
        } else {
            None
        }
    }

    /* Used during the build phase */

    /// Returns a new `TapOrHold` with the field `permissive_mod_for_tap_action` set to
    /// the given value.
    pub fn with_permissive_mod_for_tap_action(self, permissive_mod_for_tap_action: bool) -> Self {
        Self {
            permissive_mod_for_tap_action: Some(permissive_mod_for_tap_action),
            ..self
        }
    }

    /// Returns a new `TapOrHold` with the field `with_preemptive_mod_hold` set to
    /// the given value.
    pub fn with_preemptive_mod_hold(self, preemptive_mod_hold: bool) -> Self {
        Self {
            preemptive_mod_hold: Some(preemptive_mod_hold),
            ..self
        }
    }
}

impl<CustomData> fmt::Debug for TapHoldAction<CustomData> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("TapHoldAction")
            .field("tap", &self.tap)
            .field("hold", &self.hold)
            .field(
                "permissive_mod_for_tap_action",
                &self.permissive_mod_for_tap_action,
            )
            .field("preemptive_mod_hold", &self.preemptive_mod_hold)
            .finish()
    }
}

#[cfg(test)]
impl<CustomData> crate::util::SizeOf for TapHoldAction<CustomData> {
    fn size_of(arch_in_bytes: usize) -> usize {
        core::mem::size_of::<Option<bool>>() * 2
            + <TapAction<CustomData>>::size_of(arch_in_bytes)
            + <HoldAction<CustomData>>::size_of(arch_in_bytes)
    }
}
