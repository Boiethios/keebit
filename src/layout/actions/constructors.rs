#[cfg(feature = "reserved")]
use crate::hid::ReservedByte;

use crate::{
    hid::{sealed::ReportKeys, ModifiersByte, Report},
    layout::{Event, LayoutState, MonotonicTimer, Reports},
    util::MaybeU8,
};

use super::{
    Action, CustomAction, HoldAction, KeyPress, Macro, MaybeAction, ModLayer, TapAction,
    TapHoldAction,
};

// Contructors:

/// Creates a new action to send a key either on tap or
/// on hold in a tap/hold action.
pub const fn k<T>(keypresses: impl ~const ReportKeys) -> T
where
    T: ~const ConstFrom<Report>,
{
    let keypresses = keypresses.keys();
    ConstFrom::const_from(Report {
        modifiers: ModifiersByte(0),
        #[cfg(feature = "reserved")]
        reserved: ReservedByte(0),
        keypresses,
    })
}

/// Creates a new action to send a key with modifier(s) either on tap or
/// on hold in a tap/hold action.
pub const fn km<T>(keypresses: impl ~const ReportKeys, modifiers: u8) -> T
where
    T: ~const ConstFrom<Report>,
{
    let keypresses = keypresses.keys();
    ConstFrom::const_from(Report {
        modifiers: ModifiersByte(modifiers),
        #[cfg(feature = "reserved")]
        reserved: ReservedByte(0),
        keypresses,
    })
}

/// Creates an action being a regular modifier key, like Ctrl or Shift.
pub const fn m<T>(modifiers: u8) -> T
where
    T: ~const ConstFrom<ModLayer>,
{
    let mod_layer = ModLayer::new(
        ModifiersByte(modifiers),
        MaybeU8::NONE,
        #[cfg(feature = "reserved")]
        ReservedByte::ZERO,
    );
    ConstFrom::const_from(mod_layer)
}

/// Creates an action moving to a layer as long as the key is pressed.
pub const fn l<T>(layer: u8) -> T
where
    T: ~const ConstFrom<ModLayer>,
{
    let mod_layer = ModLayer::new(
        ModifiersByte(0),
        MaybeU8::from_u8(layer),
        #[cfg(feature = "reserved")]
        ReservedByte::ZERO,
    );
    ConstFrom::const_from(mod_layer)
}

/// Creates an action moving to a layer as long as the key is pressed with modifiers
/// being activated.
pub const fn ml<T>(modifiers: u8, layer: u8) -> T
where
    T: ~const ConstFrom<ModLayer>,
{
    let mod_layer = ModLayer::new(
        ModifiersByte(modifiers),
        MaybeU8::from_u8(layer),
        #[cfg(feature = "reserved")]
        ReservedByte::ZERO,
    );
    ConstFrom::const_from(mod_layer)
}

/// Creates a one-shot version of [`m`]: once another key is pressed, the modifier
/// is deactivated.
pub const fn m1<T, CustomData>(modifiers: u8) -> T
where
    T: ~const ConstFrom<TapAction<CustomData>>,
{
    let tap_action = TapAction::OneShot(ModLayer::new(
        ModifiersByte(modifiers),
        MaybeU8::NONE,
        #[cfg(feature = "reserved")]
        ReservedByte::ZERO,
    ));
    ConstFrom::const_from(tap_action)
}

/// Creates a one-shot version of [`l`]: once another key is pressed, the active layer
/// is back to the original one.
pub const fn l1<T, CustomData>(layer: u8) -> T
where
    T: ~const ConstFrom<TapAction<CustomData>>,
{
    let tap_action = TapAction::OneShot(ModLayer::new(
        ModifiersByte(0),
        MaybeU8::from_u8(layer),
        #[cfg(feature = "reserved")]
        ReservedByte::ZERO,
    ));
    ConstFrom::const_from(tap_action)
}

/// Creates a one-shot version of [`ml`]: once another key is pressed, the active layer
/// is back to the original one, and the modifier is deactivated.
pub const fn ml1<T, CustomData>(modifiers: u8, layer: u8) -> T
where
    T: ~const ConstFrom<TapAction<CustomData>>,
{
    let tap_action = TapAction::OneShot(ModLayer::new(
        ModifiersByte(modifiers),
        MaybeU8::from_u8(layer),
        #[cfg(feature = "reserved")]
        ReservedByte::ZERO,
    ));
    ConstFrom::const_from(tap_action)
}

/// Creates a swich version of [`m`]: the modifier
/// is active until the user deactivates it manually.
pub const fn sw_m<T, CustomData>(_modifiers: u8) -> T
where
    T: ~const ConstFrom<TapAction<CustomData>>,
{
    panic!("not yet implemented")
}

/// Creates a swich version of [`l`]: the layer
/// is active until the user deactivates it manually.
pub const fn sw_l<T, CustomData>(_layer: u8) -> T
where
    T: ~const ConstFrom<TapAction<CustomData>>,
{
    panic!("not yet implemented")
}

/// Creates a swich version of [`l`]: the layer and modifier(s)
/// are active until the user deactivates them manually.
pub const fn sw_ml<T, CustomData>(_modifiers: u8, _layer: u8) -> T
where
    T: ~const ConstFrom<TapAction<CustomData>>,
{
    panic!("not yet implemented")
}

/// Creates a new macro from several reports. They are sent in order to the host
/// when the key is pressed.
pub const fn macr<T>(reports: &'static [Report]) -> T
where
    T: ~const ConstFrom<Macro>,
{
    ConstFrom::const_from(Macro(reports))
}

/// Creates a custom user-defined action.
///
/// It has access to a lot of data on purpose, so that it allows to do basically everything.
/// This comes with the downside that it is easy to break things.
pub const fn custom<T, CustomData>(
    user_action: fn(
        reports: &mut Reports,
        state: &mut LayoutState<CustomData>,
        monotonic: &mut dyn MonotonicTimer,
        event: Event,
    ),
) -> T
where
    T: ~const ConstFrom<CustomAction<CustomData>>,
{
    ConstFrom::const_from(CustomAction { user_action })
}

/// Creates a tap-or-hold action.
pub const fn hold<T, CustomData>(
    on_tap: TapAction<CustomData>,
    on_hold: HoldAction<CustomData>,
) -> T
where
    T: ~const ConstFrom<TapHoldAction<CustomData>>,
{
    let tap_hold_action = TapHoldAction {
        tap: on_tap,
        hold: on_hold,
        permissive_mod_for_tap_action: None,
        preemptive_mod_hold: None,
    };
    ConstFrom::const_from(tap_hold_action)
}

// Implementations:

// --- From Report

impl<CustomData> const ConstFrom<Report> for MaybeAction<CustomData> {
    fn const_from(report: Report) -> Self {
        MaybeAction::Action(Action::Tap(TapAction::KeyPress(KeyPress(report))))
    }
}

impl<CustomData> const ConstFrom<Report> for Action<CustomData> {
    fn const_from(report: Report) -> Self {
        Action::Tap(TapAction::KeyPress(KeyPress(report)))
    }
}

impl<CustomData> const ConstFrom<Report> for TapAction<CustomData> {
    fn const_from(report: Report) -> Self {
        TapAction::KeyPress(KeyPress(report))
    }
}

impl<CustomData> const ConstFrom<Report> for HoldAction<CustomData> {
    fn const_from(report: Report) -> Self {
        HoldAction::KeyPress(KeyPress(report))
    }
}

impl const ConstFrom<Report> for KeyPress {
    fn const_from(report: Report) -> Self {
        KeyPress(report)
    }
}

// --- From KeyPress

impl<CustomData> const ConstFrom<KeyPress> for MaybeAction<CustomData> {
    fn const_from(key_press: KeyPress) -> Self {
        MaybeAction::Action(Action::Tap(TapAction::KeyPress(key_press)))
    }
}

impl<CustomData> const ConstFrom<KeyPress> for Action<CustomData> {
    fn const_from(key_press: KeyPress) -> Self {
        Action::Tap(TapAction::KeyPress(key_press))
    }
}

impl<CustomData> const ConstFrom<KeyPress> for TapAction<CustomData> {
    fn const_from(key_press: KeyPress) -> Self {
        TapAction::KeyPress(key_press)
    }
}

impl<CustomData> const ConstFrom<KeyPress> for HoldAction<CustomData> {
    fn const_from(key_press: KeyPress) -> Self {
        HoldAction::KeyPress(key_press)
    }
}

// --- From ModLayer (regular hold)

impl<CustomData> const ConstFrom<ModLayer> for MaybeAction<CustomData> {
    fn const_from(mod_layer: ModLayer) -> Self {
        MaybeAction::Action(Action::Hold(mod_layer))
    }
}

impl<CustomData> const ConstFrom<ModLayer> for Action<CustomData> {
    fn const_from(mod_layer: ModLayer) -> Self {
        Action::Hold(mod_layer)
    }
}

impl<CustomData> const ConstFrom<ModLayer> for HoldAction<CustomData> {
    fn const_from(mod_layer: ModLayer) -> Self {
        HoldAction::ModLayer(mod_layer)
    }
}

// --- From Macro

impl<CustomData> const ConstFrom<Macro> for MaybeAction<CustomData> {
    fn const_from(macro_action: Macro) -> Self {
        MaybeAction::Action(Action::Tap(TapAction::Macro(macro_action)))
    }
}

impl<CustomData> const ConstFrom<Macro> for Action<CustomData> {
    fn const_from(macro_action: Macro) -> Self {
        Action::Tap(TapAction::Macro(macro_action))
    }
}

impl<CustomData> const ConstFrom<Macro> for TapAction<CustomData> {
    fn const_from(macro_action: Macro) -> Self {
        TapAction::Macro(macro_action)
    }
}

impl<CustomData> const ConstFrom<Macro> for HoldAction<CustomData> {
    fn const_from(macro_action: Macro) -> Self {
        HoldAction::Macro(macro_action)
    }
}

// --- From CustomAction

impl<CustomData> const ConstFrom<CustomAction<CustomData>> for MaybeAction<CustomData> {
    fn const_from(custom_action: CustomAction<CustomData>) -> Self {
        MaybeAction::Action(Action::Tap(TapAction::Custom(custom_action)))
    }
}

impl<CustomData> const ConstFrom<CustomAction<CustomData>> for Action<CustomData> {
    fn const_from(custom_action: CustomAction<CustomData>) -> Self {
        Action::Tap(TapAction::Custom(custom_action))
    }
}

impl<CustomData> const ConstFrom<CustomAction<CustomData>> for TapAction<CustomData> {
    fn const_from(custom_action: CustomAction<CustomData>) -> Self {
        TapAction::Custom(custom_action)
    }
}

impl<CustomData> const ConstFrom<CustomAction<CustomData>> for HoldAction<CustomData> {
    fn const_from(custom_action: CustomAction<CustomData>) -> Self {
        HoldAction::Custom(custom_action)
    }
}

// --- From TapAction

impl<CustomData> const ConstFrom<TapAction<CustomData>> for MaybeAction<CustomData> {
    fn const_from(tap_action: TapAction<CustomData>) -> Self {
        MaybeAction::Action(Action::Tap(tap_action))
    }
}

impl<CustomData> const ConstFrom<TapAction<CustomData>> for Action<CustomData> {
    fn const_from(tap_action: TapAction<CustomData>) -> Self {
        Action::Tap(tap_action)
    }
}

// --- From HoldAction

// --- From TapHoldAction

impl<CustomData> const ConstFrom<TapHoldAction<CustomData>> for MaybeAction<CustomData> {
    fn const_from(tap_hold_action: TapHoldAction<CustomData>) -> Self {
        MaybeAction::Action(Action::TapOrHold(tap_hold_action))
    }
}

impl<CustomData> const ConstFrom<TapHoldAction<CustomData>> for Action<CustomData> {
    fn const_from(tap_hold_action: TapHoldAction<CustomData>) -> Self {
        Action::TapOrHold(tap_hold_action)
    }
}

// From:

/// Same as `From` from std, but can be used in const context.
#[const_trait]
pub trait ConstFrom<T> {
    /// Convert from the given value.
    fn const_from(value: T) -> Self;
}

impl<T> ConstFrom<T> for T {
    fn const_from(value: T) -> Self {
        value
    }
}

// Into:

/// Same as `Into` from std, but can be used in const context.
#[const_trait]
pub trait ConstInto<T>: Sized {
    /// Convert to the given type.
    fn const_into(self) -> T;
}

impl<T, U> ConstInto<U> for T
where
    U: ConstFrom<T>,
{
    fn const_into(self) -> U {
        U::const_from(self)
    }
}
