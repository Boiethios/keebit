use crate::{
    hid::ModifiersByte,
    layout::{actions_mut::ActionMutData, LayoutState, Reports},
    state::{Event, EventKind},
    timer::MonotonicTimer,
    util::{activate_reserved_bits, deactivate_reserved_bits, MaybeU8},
};

#[cfg(feature = "reserved")]
use crate::hid::ReservedByte;

/// A regular set of modifiers, that can be empty, and an optional layer change.
///
/// # Features
///
/// When the macos feature is active, it holds the reserved byte as well,
/// to be able to activate the “fn” key.
#[derive(Debug, Clone, Copy)]
pub struct ModLayer {
    /// The modifiers for this action (CTRL, ALT, etc.)
    pub modifiers: ModifiersByte,
    layer: MaybeU8,
    #[cfg(feature = "reserved")]
    reserved: ReservedByte,
}

impl ModLayer {
    /// Creates a new `ModLayer`.
    pub const fn new(
        modifiers: ModifiersByte,
        layer: MaybeU8,
        #[cfg(feature = "reserved")] reserved: ReservedByte,
    ) -> Self {
        Self {
            modifiers,
            layer,
            #[cfg(feature = "reserved")]
            reserved,
        }
    }

    /// Adds the reports to be sent to the host to `reports`.
    pub fn act<CustomData>(
        &self,
        _reports: &mut Reports,
        _data_mut: &mut ActionMutData,
        state: &mut LayoutState<CustomData>,
        _monotonic: &mut impl MonotonicTimer,
        event: Event,
    ) {
        let ModLayer {
            modifiers, layer, ..
        } = *self;

        match event.kind {
            EventKind::Pressed => {
                state.strong_modifiers += modifiers;
                if let Some(layer) = layer.get() {
                    state.active_layers.push(layer)
                }

                activate_reserved_bits(state, self.reserved());
            }
            EventKind::Released => {
                state.strong_modifiers -= modifiers;
                if let Some(layer) = layer.get() {
                    state.active_layers.remove(layer)
                }

                deactivate_reserved_bits(state, self.reserved());
            }
        }
    }

    /// Cleanup the state after the oneshot modifier has being used.
    pub fn oneshot_cleanup<CustomData>(&self, state: &mut LayoutState<CustomData>) {
        state.strong_modifiers -= self.modifiers;
        if let Some(layer) = self.layer() {
            state.active_layers.remove(layer)
        }

        deactivate_reserved_bits(state, self.reserved());

        crate::trace!(
            "ModLayer cleaned up the one-shot action:\n\tModifiers: {:?}\n\tLayers: {:?}",
            state.strong_modifiers,
            state.active_layers
        );
    }

    /// The layer this action send to.
    pub fn layer(&self) -> Option<u8> {
        self.layer.get()
    }

    #[cfg(not(feature = "reserved"))]
    pub(crate) const fn reserved(&self) -> u8 {
        0
    }
    /// The reserved byte in the HID report.
    #[cfg(feature = "reserved")]
    pub fn reserved(&self) -> u8 {
        self.reserved.0
    }
}
