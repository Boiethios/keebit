use crate::{
    hid::Report,
    layout::{actions_mut::ActionMutData, LayoutState, Reports},
    state::{Event, EventKind},
    timer::MonotonicTimer,
};

/// A macro action sending several reports to the host in succession.
#[derive(Debug, Clone, Copy)]
pub struct Macro(pub &'static [Report]);

impl Macro {
    /// Adds the reports to be sent to the host to `reports`.
    pub fn act<CustomData>(
        &self,
        reports: &mut Reports,
        _data_mut: &mut ActionMutData,
        _state: &mut LayoutState<CustomData>,
        _monotonic: &mut impl MonotonicTimer,
        event: Event,
    ) {
        match event.kind {
            EventKind::Pressed => {
                let space_to_use = self.0.len() * 2;
                // If there is not enough free space, just do nothing.
                // This is a corner case anyway:
                if space_to_use + reports.len() <= reports.capacity() {
                    for &report in self.0 {
                        let _ = reports.push(report);
                        let _ = reports.push(Report::empty());
                    }
                }
            }
            EventKind::Released => {
                // Nothing to do.
            }
        }
    }
}
