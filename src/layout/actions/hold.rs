use core::fmt;

use crate::{
    layout::actions::{CustomAction, KeyPress, Macro, ModLayer},
    layout::{actions_mut::ActionMutData, Event, LayoutState, MonotonicTimer, Reports},
};

/// An action that can happen when a key is held in a tap-or-hold action.
pub enum HoldAction<CustomData = ()> {
    /// Add a report to the main one.
    KeyPress(KeyPress),
    /// Activates a modifier or a layer change. It is deactivated when the key is pressed again.
    ModLayer(ModLayer),
    /// Macro action.
    Macro(Macro),
    /// Custom action.
    Custom(CustomAction<CustomData>),
}

impl<CustomData> HoldAction<CustomData> {
    pub(crate) fn act(
        &self,
        reports: &mut Reports,
        data_mut: &mut ActionMutData,
        state: &mut LayoutState<CustomData>,
        monotonic: &mut impl MonotonicTimer,
        event: Event,
    ) {
        crate::trace!("HoldAction {} handles event {event:?}", self.discr_name());

        match self {
            HoldAction::KeyPress(action) => action.act(reports, data_mut, state, monotonic, event),
            HoldAction::ModLayer(action) => action.act(reports, data_mut, state, monotonic, event),
            HoldAction::Macro(action) => action.act(reports, data_mut, state, monotonic, event),
            HoldAction::Custom(action) => action.act(reports, data_mut, state, monotonic, event),
        }
    }

    #[cfg(feature = "tracing")]
    fn discr_name(&self) -> &'static str {
        match self {
            HoldAction::KeyPress(_) => "KeyPress",
            HoldAction::ModLayer(_) => "ModLayer",
            HoldAction::Macro(_) => "Macro",
            HoldAction::Custom(_) => "Custom",
        }
    }
}

impl<CustomData> fmt::Debug for HoldAction<CustomData> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::KeyPress(arg0) => f.debug_tuple("KeyPress").field(arg0).finish(),
            Self::ModLayer(arg0) => f.debug_tuple("ModLayer").field(arg0).finish(),
            Self::Macro(arg0) => f.debug_tuple("Macro").field(arg0).finish(),
            Self::Custom(arg0) => f.debug_tuple("Custom").field(arg0).finish(),
        }
    }
}

#[cfg(test)]
impl<CustomData> crate::util::SizeOf for HoldAction<CustomData> {
    fn size_of(arch_in_bytes: usize) -> usize {
        let largest_variant = core::mem::size_of::<KeyPress>()
            .max(core::mem::size_of::<ModLayer>())
            .max(core::mem::size_of::<CustomAction<CustomData>>());

        arch_in_bytes + largest_variant
    }
}
