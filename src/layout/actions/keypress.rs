use crate::{
    hid::Report,
    layout::{actions_mut::ActionMutData, LayoutState, Reports},
    state::{Event, EventKind},
    timer::MonotonicTimer,
};
use core::ops::Not;

/// A regular key press, that can accept (weak) modifiers.
#[derive(Debug, Clone, Copy)]
pub struct KeyPress(pub Report);

impl KeyPress {
    /// Adds the reports to be sent to the host to `reports`.
    pub fn act<CustomData>(
        &self,
        reports: &mut Reports,
        _data_mut: &mut ActionMutData,
        state: &mut LayoutState<CustomData>,
        _monotonic: &mut impl MonotonicTimer,
        event: Event,
    ) {
        if self.0.is_empty().not() {
            match event.kind {
                EventKind::Pressed if state.report.free() >= self.0.len() => {
                    state.report.modifiers = self.0.modifiers;
                    for key in self.0.iter_keys() {
                        state.report.add_key(key);
                    }

                    #[cfg(not(feature = "reserved"))]
                    let report = state.report.with_mod(state.strong_modifiers);
                    #[cfg(feature = "reserved")]
                    let report = state
                        .report
                        .with_mod(state.strong_modifiers)
                        .with_activated_reserved_bits(state.strong_reserved_byte);
                    let _ = reports.push(report);
                }
                EventKind::Pressed => {
                    // There is no free space in the report. Ignore for now, but maybe we'll need
                    // a configuration to set “Phantom“ instead.
                }
                EventKind::Released => {
                    for key in self.0.iter_keys() {
                        state.report.remove_key(key);
                    }
                    if state.report.is_empty() {
                        // If the report is non-empty, we should still apply the weak
                        // modifier, because it is still used:
                        state.report.modifiers = Default::default();
                    }
                }
            }
        }
    }
}
