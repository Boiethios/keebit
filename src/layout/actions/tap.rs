use crate::{
    layout::actions::{CustomAction, KeyPress, Macro, ModLayer},
    layout::{actions_mut::ActionMutData, Event, LayoutState, MonotonicTimer, Reports},
};
use core::fmt;

/// A tap action: an action that can happen when a key is tapped without being held.
pub enum TapAction<CustomData = ()> {
    /// Add a report to the main one.
    KeyPress(KeyPress),
    /// Activates a modifier or a layer change. When another key is pressed, it is deactivated.
    OneShot(ModLayer),
    /// Activates a modifier or a layer change. It is deactivated when the key is pressed again.
    Switch(ModLayer),
    /// Macro action.
    Macro(Macro),
    /// Custom action.
    Custom(CustomAction<CustomData>),
}

impl<CustomData> TapAction<CustomData> {
    pub(crate) fn act(
        &self,
        reports: &mut Reports,
        data_mut: &mut ActionMutData,
        state: &mut LayoutState<CustomData>,
        monotonic: &mut impl MonotonicTimer,
        event: Event,
    ) {
        crate::trace!("TapAction {} handles event {event:?}", self.discr_name());

        match self {
            TapAction::KeyPress(action) => action.act(reports, data_mut, state, monotonic, event),
            TapAction::OneShot(action) if event.is_pressed() => {
                state.current_one_shot_action = Some((state.active_layers.active(), event.place));
                action.act(reports, data_mut, state, monotonic, event)
            }
            // When the one-shot action is released, nothing happens:
            TapAction::OneShot(_action) => (),
            TapAction::Switch(_action) => unimplemented!(),
            TapAction::Macro(action) => action.act(reports, data_mut, state, monotonic, event),
            TapAction::Custom(action) => action.act(reports, data_mut, state, monotonic, event),
        }
    }

    #[cfg(feature = "tracing")]
    fn discr_name(&self) -> &'static str {
        match self {
            TapAction::KeyPress(_) => "KeyPress",
            TapAction::OneShot(_) => "OneShot",
            TapAction::Switch(_) => "Switch",
            TapAction::Custom(_) => "Custom",
        }
    }
}

impl<CustomData> fmt::Debug for TapAction<CustomData> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::KeyPress(arg0) => f.debug_tuple("KeyPress").field(arg0).finish(),
            Self::OneShot(arg0) => f.debug_tuple("OneShot").field(arg0).finish(),
            Self::Switch(arg0) => f.debug_tuple("Switch").field(arg0).finish(),
            Self::Macro(arg0) => f.debug_tuple("Macro").field(arg0).finish(),
            Self::Custom(arg0) => f.debug_tuple("Custom").field(arg0).finish(),
        }
    }
}

#[cfg(test)]
impl<CustomData> crate::util::SizeOf for TapAction<CustomData> {
    fn size_of(arch_in_bytes: usize) -> usize {
        let largest_variant = core::mem::size_of::<KeyPress>()
            .max(core::mem::size_of::<ModLayer>())
            .max(core::mem::size_of::<CustomAction<CustomData>>());

        arch_in_bytes + largest_variant
    }
}
