use crate::{
    layout::{actions_mut::ActionMutData, LayoutState, Reports},
    prelude::MonotonicTimer,
    state::Event,
};
use core::fmt;

/// A custom action, defined by the user.
pub struct CustomAction<CustomData> {
    /// This action will be called everytime there is an event.
    pub user_action: fn(
        reports: &mut Reports,
        state: &mut LayoutState<CustomData>,
        monotonic: &mut dyn MonotonicTimer,
        event: Event,
    ),
}

impl<CustomData> CustomAction<CustomData> {
    /// Adds the reports to be sent to the host to `reports` (or removes them,
    /// if you feel adventurous).
    pub fn act(
        &self,
        reports: &mut Reports,
        _data_mut: &mut ActionMutData,
        state: &mut LayoutState<CustomData>,
        monotonic: &mut dyn MonotonicTimer,
        event: Event,
    ) {
        (self.user_action)(reports, state, monotonic, event)
    }
}

impl<CustomData> fmt::Debug for CustomAction<CustomData> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("CustomAction")
            .field("user_action", &self.user_action)
            .finish()
    }
}
