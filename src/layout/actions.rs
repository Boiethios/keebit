use core::fmt::{self, Debug};

mod constructors;
pub use constructors::{
    custom, hold, k, km, l, l1, m, m1, macr, ml, ml1, sw_l, sw_m, sw_ml, ConstFrom, ConstInto,
};

mod tap;
pub use tap::TapAction;
mod hold;
pub use hold::HoldAction;
mod tap_hold;
pub use tap_hold::TapHoldAction;

mod keypress;
pub use keypress::KeyPress;
mod mod_layer;
pub use mod_layer::ModLayer;
mod custom;
pub use custom::CustomAction;
mod macr;
pub use macr::Macro;

use super::*;

/// A layer slot that may be an action. It is either:
/// - An action;
/// - An empty slot,
/// - Transparent, meaning that it will activate the action in the layer below.
#[derive(Debug)]
pub enum MaybeAction<CustomData = ()> {
    /// There is no key here.
    ///
    /// Used both to indicate that this matrix node cannot be activated
    /// and to block the transparency mechanism.
    Nothing,
    /// Looks to the layout below.
    Transparent,
    /// An action.
    Action(Action<CustomData>),
}

/// Keys action handler.
pub enum Action<CustomData = ()> {
    /// A tap action: an action that can happen when a key is tapped without being held.
    Tap(TapAction<CustomData>),
    /// An hold action: that is a modifier or a layer change.
    Hold(ModLayer),
    /// A tap or hold action: an action that has a different behavior, depending on weither it has
    /// been tapped or held.
    TapOrHold(TapHoldAction<CustomData>),
}

impl<CustomData> MaybeAction<CustomData> {
    /// Returns an optional action from a [`MaybeAction`].
    pub fn action(&self) -> Option<&Action<CustomData>> {
        match self {
            MaybeAction::Action(action) => Some(action),
            MaybeAction::Nothing | MaybeAction::Transparent => None,
        }
    }
}

impl<CustomData> Action<CustomData> {
    /// After handling an event, an action
    /// - may modify the internal state of the layout;
    /// - may return a report to send to the host.
    #[inline(always)]
    pub(crate) fn handle_event(
        &self,
        data_mut: &mut ActionMutData,
        state: &mut LayoutState<CustomData>,
        monotonic: &mut impl MonotonicTimer,
        event: Event,
    ) -> super::Reports {
        crate::trace!("---\nAction {} handles event {event:?}", self.discr_name());

        let mut reports = Default::default();

        match self {
            Action::Tap(tap_action) => {
                tap_action.act(&mut reports, data_mut, state, monotonic, event)
            }
            Action::Hold(hold_action) => {
                hold_action.act(&mut reports, data_mut, state, monotonic, event)
            }
            Action::TapOrHold(tap_hold_action) => {
                tap_hold_action.act(&mut reports, data_mut, state, monotonic, event)
            }
        }

        reports
    }

    /// Cleans up the one-shot actions.
    #[inline(always)]
    pub(crate) fn cleanup(
        &self,
        _data_mut: &mut ActionMutData,
        state: &mut LayoutState<CustomData>,
    ) {
        use Action::*;

        match self {
            Tap(TapAction::OneShot(action)) => action.oneshot_cleanup(state),
            _ => (),
        }
    }

    #[cfg(feature = "tracing")]
    fn discr_name(&self) -> &'static str {
        match self {
            Action::Tap(_) => "Tap",
            Action::Hold(_) => "Hold",
            Action::TapOrHold(_) => "TapOrHold",
        }
    }
}

impl<CustomData> fmt::Debug for Action<CustomData> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Tap(arg0) => f.debug_tuple("Tap").field(arg0).finish(),
            Self::Hold(arg0) => f.debug_tuple("Hold").field(arg0).finish(),
            Self::TapOrHold(arg0) => f.debug_tuple("TapOrHold").field(arg0).finish(),
        }
    }
}

#[cfg(test)]
impl<CustomData> crate::util::SizeOf for Action<CustomData> {
    fn size_of(arch_in_bytes: usize) -> usize {
        let largest_variant = <TapAction<u64>>::size_of(arch_in_bytes)
            .max(core::mem::size_of::<ModLayer>())
            .max(<TapHoldAction<u64>>::size_of(arch_in_bytes));

        arch_in_bytes + largest_variant
    }
}

#[test]
fn anti_size_regression_verification() {
    use crate::util::SizeOf;

    // Verify that the `MaybeAction` has the same size as `Action`, that is,
    // that the enum variant optimization kicks in:
    assert_eq!(
        core::mem::size_of::<Action<()>>(),
        core::mem::size_of::<MaybeAction<()>>(),
        "size of Action should be the same as size of MaybeAction"
    );

    assert_eq!(
        <Action<u64>>::size_of(2),
        <Action<()>>::size_of(2),
        "size of Action should not depend on CustomData"
    );

    assert_eq!(24, <Action<()>>::size_of(2), "size of Action changed");
}
