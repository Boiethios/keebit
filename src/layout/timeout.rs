use crate::{prelude::MonotonicTimer, state::EventPlace};

#[derive(Debug, Default)]
pub struct Timeout {
    elapsed_at: u64,
    // None if no timeout is ongoing.
    place: Option<EventPlace>,
}

impl Timeout {
    pub(crate) fn set(
        &mut self,
        monotonic: &mut impl MonotonicTimer,
        place: EventPlace,
        length: u64,
    ) {
        *self = Timeout {
            elapsed_at: monotonic.now().wrapping_add(length),
            place: Some(place),
        };
    }

    /// Returns the place where the timeout has elapsed, if it has.
    pub(crate) fn elapsed(&mut self, monotonic: &mut impl MonotonicTimer) -> Option<EventPlace> {
        if monotonic.now() >= self.elapsed_at {
            self.place.take()
        } else {
            None
        }
    }

    pub(crate) fn take(&mut self) -> Option<EventPlace> {
        self.place.take()
    }

    /// Returns the ongoing coordinates of the timeout (if any) and resets it.
    pub fn ongoing(&mut self) -> Option<EventPlace> {
        self.place
    }

    pub fn clear(&mut self) {
        self.place = None;
    }
}
