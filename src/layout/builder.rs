use crate::{
    chords::{Chord, Chords, Key},
    coord::Coord,
    error::Error,
    layout::{Layout, MaybeAction},
    state::StateHandler,
    timer::MonotonicTimer,
    util::try_collect,
};

use super::{actions::Action, ActiveLayers};

#[allow(missing_debug_implementations)]
pub struct LayoutBuilder<
    T,
    D,
    CustomData: 'static,
    const CHORDS: usize,
    const MAX_KEYS_PER_CHORD: usize,
    const RETURNED_REPORTS: usize,
    const N: usize,
    const ROW: usize,
    const COL: usize,
> {
    read_only_layers_info: &'static [[[MaybeAction<CustomData>; COL]; ROW]; N],
    monotonic_timer: T,
    debounce_time: D,
    #[allow(clippy::type_complexity)]
    chords: [(&'static [(u8, u8)], Action<CustomData>); CHORDS],

    base_layer: u8,
    below: Option<fn(u8) -> Option<u8>>,
    default_timeout: u64,
    permissive_mod_for_tap_action: bool,
    preemptive_mod_hold: bool,

    custom_data: CustomData,
}

#[derive(Debug)]
pub struct Timer<Mt>(Mt);

/// Meant to be used as a generic parameter for expressing that there is no timer in the layout.
#[derive(Debug)]
pub struct NoTimer;

#[derive(Debug)]
pub struct Debouncer(u64);
#[derive(Debug)]
pub struct NoDebouncer;

impl<CustomData, const N: usize, const ROW: usize, const COL: usize>
    Layout<NoTimer, CustomData, 0, 0, N, ROW, COL>
{
    /// Creates a new layout builder.
    pub fn builder(
        custom_data: CustomData,
        read_only_layers_info: &'static [[[MaybeAction<CustomData>; COL]; ROW]; N],
    ) -> LayoutBuilder<NoTimer, NoDebouncer, CustomData, 0, 0, 32, N, ROW, COL> {
        LayoutBuilder {
            read_only_layers_info,
            monotonic_timer: NoTimer,
            debounce_time: NoDebouncer,
            chords: [],

            base_layer: 0,
            below: None,
            default_timeout: 250,
            permissive_mod_for_tap_action: true,
            preemptive_mod_hold: true,

            custom_data,
        }
    }
}

// Available in any state:
impl<
        T,
        D,
        CustomData,
        const CHORDS: usize,
        const MAX_KEYS_PER_CHORD: usize,
        const RETURNED_REPORTS: usize,
        const N: usize,
        const ROW: usize,
        const COL: usize,
    > LayoutBuilder<T, D, CustomData, CHORDS, MAX_KEYS_PER_CHORD, RETURNED_REPORTS, N, ROW, COL>
{
    pub fn with_below(self, below: fn(u8) -> Option<u8>) -> Self {
        Self {
            below: Some(below),
            ..self
        }
    }

    /// Change the active layer at the start of the library. It is an advanced feature,
    /// use it if you know what you do, otherwise, it could lead to weird behaviors.
    pub fn with_starting_base_layer(self, base_layer: u8) -> Self {
        Self { base_layer, ..self }
    }
}

// A timer can be added if there is not one already:
impl<
        CustomData,
        const CHORDS: usize,
        const MAX_KEYS_PER_CHORD: usize,
        const RETURNED_REPORTS: usize,
        const N: usize,
        const ROW: usize,
        const COL: usize,
    >
    LayoutBuilder<
        NoTimer,
        NoDebouncer,
        CustomData,
        CHORDS,
        MAX_KEYS_PER_CHORD,
        RETURNED_REPORTS,
        N,
        ROW,
        COL,
    >
{
    pub fn with_timer<Monotonic: MonotonicTimer>(
        self,
        timer: Monotonic,
    ) -> LayoutBuilder<
        Timer<Monotonic>,
        NoDebouncer,
        CustomData,
        CHORDS,
        MAX_KEYS_PER_CHORD,
        RETURNED_REPORTS,
        N,
        ROW,
        COL,
    > {
        LayoutBuilder {
            read_only_layers_info: self.read_only_layers_info,
            monotonic_timer: Timer(timer),
            debounce_time: self.debounce_time,
            chords: self.chords,
            base_layer: self.base_layer,
            below: self.below,
            default_timeout: self.default_timeout,
            permissive_mod_for_tap_action: self.permissive_mod_for_tap_action,
            preemptive_mod_hold: self.preemptive_mod_hold,
            custom_data: self.custom_data,
        }
    }
}

// Config available when there is a timer:
impl<
        D,
        CustomData,
        const CHORDS: usize,
        const MAX_KEYS_PER_CHORD: usize,
        const RETURNED_REPORTS: usize,
        Monotonic,
        const N: usize,
        const ROW: usize,
        const COL: usize,
    >
    LayoutBuilder<
        Timer<Monotonic>,
        D,
        CustomData,
        CHORDS,
        MAX_KEYS_PER_CHORD,
        RETURNED_REPORTS,
        N,
        ROW,
        COL,
    >
{
    /// Adds a default timeout for the tap-hold actions.
    pub fn with_default_timeout(
        self,
        default_timeout: u64,
    ) -> LayoutBuilder<
        Timer<Monotonic>,
        D,
        CustomData,
        CHORDS,
        MAX_KEYS_PER_CHORD,
        RETURNED_REPORTS,
        N,
        ROW,
        COL,
    > {
        LayoutBuilder {
            read_only_layers_info: self.read_only_layers_info,
            monotonic_timer: self.monotonic_timer,
            debounce_time: self.debounce_time,
            chords: self.chords,
            base_layer: self.base_layer,
            below: self.below,
            default_timeout,
            permissive_mod_for_tap_action: self.permissive_mod_for_tap_action,
            preemptive_mod_hold: self.preemptive_mod_hold,
            custom_data: self.custom_data,
        }
    }

    /// Adds weither a mod applies a tap (in a tap/hold context) during the press,
    /// or during the release.
    pub fn with_permissive_mod_for_tap_action(
        self,
        permissive_mod_for_tap_action: bool,
    ) -> LayoutBuilder<
        Timer<Monotonic>,
        D,
        CustomData,
        CHORDS,
        MAX_KEYS_PER_CHORD,
        RETURNED_REPORTS,
        N,
        ROW,
        COL,
    > {
        LayoutBuilder {
            read_only_layers_info: self.read_only_layers_info,
            monotonic_timer: self.monotonic_timer,
            debounce_time: self.debounce_time,
            chords: self.chords,
            base_layer: self.base_layer,
            below: self.below,
            default_timeout: self.default_timeout,
            permissive_mod_for_tap_action,
            preemptive_mod_hold: self.preemptive_mod_hold,
            custom_data: self.custom_data,
        }
    }

    /// Adds weither a mod applies a tap (in a tap/hold context) during the press,
    /// or during the release.
    pub fn with_preemptive_mod_hold(
        self,
        preemptive_mod_hold: bool,
    ) -> LayoutBuilder<
        Timer<Monotonic>,
        D,
        CustomData,
        CHORDS,
        MAX_KEYS_PER_CHORD,
        RETURNED_REPORTS,
        N,
        ROW,
        COL,
    > {
        LayoutBuilder {
            read_only_layers_info: self.read_only_layers_info,
            monotonic_timer: self.monotonic_timer,
            debounce_time: self.debounce_time,
            chords: self.chords,
            base_layer: self.base_layer,
            below: self.below,
            default_timeout: self.default_timeout,
            permissive_mod_for_tap_action: self.permissive_mod_for_tap_action,
            preemptive_mod_hold,
            custom_data: self.custom_data,
        }
    }
}

// A debounce delay can be added if there is a monotonic timer:
impl<
        CustomData,
        const CHORDS: usize,
        const MAX_KEYS_PER_CHORD: usize,
        const RETURNED_REPORTS: usize,
        Monotonic,
        const N: usize,
        const ROW: usize,
        const COL: usize,
    >
    LayoutBuilder<
        Timer<Monotonic>,
        NoDebouncer,
        CustomData,
        CHORDS,
        MAX_KEYS_PER_CHORD,
        RETURNED_REPORTS,
        N,
        ROW,
        COL,
    >
{
    /// Add a debouncer to the algorithm. If you are not sure about the value,
    /// 25~30 milliseconds is a good default.
    pub fn with_debounce_time(
        self,
        debounce_time: u64,
    ) -> LayoutBuilder<
        Timer<Monotonic>,
        Debouncer,
        CustomData,
        CHORDS,
        MAX_KEYS_PER_CHORD,
        RETURNED_REPORTS,
        N,
        ROW,
        COL,
    > {
        LayoutBuilder {
            read_only_layers_info: self.read_only_layers_info,
            monotonic_timer: self.monotonic_timer,
            debounce_time: Debouncer(debounce_time),
            chords: self.chords,
            base_layer: self.base_layer,
            below: self.below,
            default_timeout: self.default_timeout,
            permissive_mod_for_tap_action: self.permissive_mod_for_tap_action,
            preemptive_mod_hold: self.preemptive_mod_hold,
            custom_data: self.custom_data,
        }
    }
}

// Once there are both a monotic timer and a debounce time set, the chords can be added:
impl<
        CustomData,
        Monotonic,
        const CHORDS1: usize,
        const MAX_KEYS_PER_CHORD1: usize,
        const RETURNED_REPORTS: usize,
        const N: usize,
        const ROW: usize,
        const COL: usize,
    >
    LayoutBuilder<
        Timer<Monotonic>,
        Debouncer,
        CustomData,
        CHORDS1,
        MAX_KEYS_PER_CHORD1,
        RETURNED_REPORTS,
        N,
        ROW,
        COL,
    >
{
    #[allow(clippy::type_complexity)]
    pub fn with_chords<const CHORDS2: usize, const MAX_KEYS_PER_CHORD: usize>(
        self,
        chords: [(&'static [(u8, u8)], Action<CustomData>); CHORDS2],
    ) -> LayoutBuilder<
        Timer<Monotonic>,
        Debouncer,
        CustomData,
        CHORDS2,
        MAX_KEYS_PER_CHORD,
        RETURNED_REPORTS,
        N,
        ROW,
        COL,
    > {
        LayoutBuilder {
            read_only_layers_info: self.read_only_layers_info,
            monotonic_timer: self.monotonic_timer,
            debounce_time: self.debounce_time,
            chords,
            base_layer: self.base_layer,
            below: self.below,
            default_timeout: self.default_timeout,
            permissive_mod_for_tap_action: self.permissive_mod_for_tap_action,
            preemptive_mod_hold: self.preemptive_mod_hold,
            custom_data: self.custom_data,
        }
    }
}

impl<
        T,
        D,
        CustomData,
        const CHORDS: usize,
        const MAX_KEYS_PER_CHORD: usize,
        const RETURNED_REPORTS: usize,
        const N: usize,
        const ROW: usize,
        const COL: usize,
    > LayoutBuilder<T, D, CustomData, CHORDS, MAX_KEYS_PER_CHORD, RETURNED_REPORTS, N, ROW, COL>
where
    D: Into<u64>,
{
    pub fn build<Monotonic>(
        self,
    ) -> Result<Layout<Monotonic, CustomData, CHORDS, MAX_KEYS_PER_CHORD, N, ROW, COL>, Error>
    where
        Monotonic: MonotonicTimer,
        T: Into<Timer<Monotonic>>,
    {
        self.validate()?;

        let state_handler = StateHandler::new(0);
        let chords = self.chords.try_map(|(keys, action)| {
            let keys = {
                let mut result =
                    try_collect::<_, MAX_KEYS_PER_CHORD>(keys.iter().copied().map(|(row, col)| {
                        Key {
                            coo: Coord::new(row, col),
                            pressed: false,
                        }
                    }))
                    .map_err(|_| Error::InvalidMaxKeysForChord)?;
                result.sort_unstable_by_key(|k| k.coo);
                result
            };

            Ok(Chord {
                keys,
                action,
                active: false,
            })
        })?;

        Ok(Layout {
            state_handler,
            layout_state: super::LayoutState {
                active_layers: ActiveLayers::new(self.base_layer),
                report: Default::default(),
                strong_modifiers: Default::default(),
                #[cfg(feature = "reserved")]
                strong_reserved_byte: Default::default(),
                current_timeout: Default::default(),
                current_one_shot_action: None,
                below: self.below,
                default_timeout: self.default_timeout,
                permissive_mod_for_tap_action: self.permissive_mod_for_tap_action,
                preemptive_mod_hold: self.preemptive_mod_hold,
                custom_data: self.custom_data,
            },
            read_only_layers_info: self.read_only_layers_info,
            mutable_layers_info: [[Default::default(); COL]; ROW],
            monotonic: self.monotonic_timer.into().0,
            debounce_time: self.debounce_time.into(),
            pressed_keys: [[Default::default(); COL]; ROW],
            chords: Chords::new(chords),
        })
    }

    pub fn validate(&self) -> Result<(), Error> {
        fn assert_that(value: bool, error: Error) -> Result<(), Error> {
            if value {
                Ok(())
            } else {
                Err(error)
            }
        }

        /*
        /// 0   0   1
        /// 0   1   1
        /// 1   0   0
        /// 1   1   1
        ///
        /// ¬P ∨ Q
        pub const fn imply(p: bool, q: bool) -> bool {
            !p | q
        }
        */

        let max_keys_per_chord = self
            .chords
            .iter()
            .map(|(keys, _)| keys.len())
            .max()
            .unwrap_or(0);
        //let there_are_chords = CHORDS != 0;

        // Check that:
        // - There is enough space for the user keys for each chord;
        // - It's tightly adjusted (not too many space).
        assert_that(
            max_keys_per_chord == MAX_KEYS_PER_CHORD,
            Error::InvalidMaxKeysForChord,
        )?;

        Ok(())
    }
}

impl From<NoTimer> for Timer<NoTimer> {
    fn from(_value: NoTimer) -> Self {
        Timer(NoTimer)
    }
}

impl From<NoDebouncer> for u64 {
    fn from(_value: NoDebouncer) -> Self {
        0
    }
}
impl From<Debouncer> for u64 {
    fn from(value: Debouncer) -> Self {
        value.0
    }
}
