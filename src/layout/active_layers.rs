use crate::util::MaybeU8;
use core::fmt;

/// The currently active layers. The layers activation acts like a stack: the default one
pub struct ActiveLayers<const MAX: usize> {
    base: u8,
    stack: [MaybeU8; MAX],
    cached: u8,
}

impl<const MAX: usize> ActiveLayers<MAX> {
    pub(crate) fn new(base: u8) -> Self {
        Self {
            base,
            stack: [MaybeU8::NONE; MAX],
            cached: base,
        }
    }

    /// Activates a new layer.
    pub fn push(&mut self, layer: u8) {
        let layer = MaybeU8::from(layer);
        //if let Some(slot) = self.0.iter_mut().find(|slot| slot.get().is_none()) {
        //    *slot = layer;
        //}
        for slot in &mut self.stack {
            if layer == *slot {
                // If the layer is already active, it is ignored the second time:
                break;
            }
            if slot.get().is_none() {
                // Add the layer to the first empty slot. If there is no empty slot, it is ignored:
                *slot = layer;
                break;
            }
        }

        self.set_cache()
    }

    /// Deactivates a previously activated layer.
    pub fn remove(&mut self, layer: u8) {
        let layer = MaybeU8::from(layer);
        if let Some(to_remove) = self.stack.iter().position(move |&slot| layer == slot) {
            let len = self.stack.len();
            self.stack
                .copy_within(len.min(to_remove + 1)..len, to_remove);
            *self.stack.last_mut().unwrap() = Default::default();
        }

        self.set_cache()
    }

    /// Returns the currently active layer.
    pub fn active(&self) -> u8 {
        self.cached
    }

    /// Returns the default layer, when no layer is active.
    pub fn base(&self) -> u8 {
        self.base
    }

    /// Changes the default layer to a new one.
    pub fn change_base(&mut self, new_default: u8) {
        self.base = new_default;
        self.set_cache()
    }

    /// The current layer is read a lot. To not recompute it every time, it is cached.
    ///
    /// It is a small optimization, but it adds up.
    fn set_cache(&mut self) {
        self.cached = self.base;
        let mut it = self.stack.into_iter();
        while let Some(layer) = it.next().and_then(|maybe_layer| maybe_layer.get()) {
            self.cached = layer;
        }
    }
}

impl<const MAX: usize> fmt::Debug for ActiveLayers<MAX> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let n_active = self
            .stack
            .iter()
            .take_while(|&layer| layer.get().is_some())
            .count();

        f.debug_struct("ActiveLayers")
            .field("base", &self.base)
            .field("stack", &&self.stack[..n_active])
            .finish()
    }
}

#[test]
fn remove_works_as_expected() {
    // Remove in the middle:
    let mut layers = ActiveLayers {
        stack: [1.into(), 2.into(), 3.into(), 4.into()],
        base: 0,
        cached: 0,
    };
    layers.set_cache();
    assert_eq!(layers.active(), 4);

    layers.remove(3);
    assert_eq!(layers.stack, [1.into(), 2.into(), 4.into(), MaybeU8::NONE]);
    assert_eq!(layers.active(), 4);

    layers.remove(4);
    assert_eq!(
        layers.stack,
        [1.into(), 2.into(), MaybeU8::NONE, MaybeU8::NONE]
    );
    assert_eq!(layers.active(), 2);

    // Remove at the end:
    let mut layers = ActiveLayers {
        stack: [1.into(), 2.into(), 3.into(), 4.into()],
        base: 0,
        cached: 0,
    };
    layers.set_cache();
    assert_eq!(layers.active(), 4);

    layers.remove(4);
    assert_eq!(layers.stack, [1.into(), 2.into(), 3.into(), MaybeU8::NONE]);
    assert_eq!(layers.active(), 3);

    // Corner-case: no value present:
    let mut layers = ActiveLayers {
        stack: [MaybeU8::NONE; 4],
        base: 0,
        cached: 0,
    };
    layers.set_cache();
    assert_eq!(layers.active(), 0);

    layers.remove(4);
    assert_eq!(layers.stack, [MaybeU8::NONE; 4]);
    assert_eq!(layers.active(), 0);
}
