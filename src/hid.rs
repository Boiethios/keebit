use crate::util::nimply;
use core::ops::{Add, AddAssign, Sub, SubAssign};

pub use modifiers_builder::ModifiersBuilder;
pub use report_builder::ReportBuilder;
#[cfg(feature = "reserved")]
pub use reserved_byte::ReservedByte;

/// A report to be sent to the host.
#[derive(Default, PartialEq, Clone, Copy)]
pub struct Report {
    /// The modifiers in the report: left of right variants of ctrl, alt, shift,
    /// gui/meta/super/win/whatever you call it.
    pub modifiers: ModifiersByte,
    #[cfg(feature = "reserved")]
    pub(crate) reserved: ReservedByte,
    pub(crate) keypresses: [KeyCode; 6],
}

/// A full modifier state: left of right variants of ctrl, alt, shift,
/// gui/meta/super/win/whatever you call it, all in one byte of space.
#[derive(Default, PartialEq, Clone, Copy)]
pub struct ModifiersByte(pub u8);

pub use modifiers_consts::*;
#[allow(non_upper_case_globals)]
pub(crate) mod modifiers_consts {
    /// Left control key.
    pub const LeftCtrl: super::ModifiersByte = super::ModifiersByte(1 << 0);
    /// Left shift key.
    pub const LeftShift: super::ModifiersByte = super::ModifiersByte(1 << 1);
    /// Left alt key.
    pub const LeftAlt: super::ModifiersByte = super::ModifiersByte(1 << 2);
    /// Left “gui” or “meta” or “super” or “windows” key.
    pub const LeftGui: super::ModifiersByte = super::ModifiersByte(1 << 3);
    /// Right control key.
    pub const RightCtrl: super::ModifiersByte = super::ModifiersByte(1 << 4);
    /// Right shift key.
    pub const RightShift: super::ModifiersByte = super::ModifiersByte(1 << 5);
    /// Right alt key.
    pub const RightAlt: super::ModifiersByte = super::ModifiersByte(1 << 6);
    /// Right “gui” or “meta” or “super” or “windows” key.
    pub const RightGui: super::ModifiersByte = super::ModifiersByte(1 << 7);
}

pub(crate) mod sealed {
    use super::{KeyCode::No, *};

    #[const_trait]
    pub trait ReportKeys {
        fn keys(self) -> [KeyCode; 6];
    }

    impl const ReportKeys for KeyCode {
        fn keys(self) -> [KeyCode; 6] {
            [self, No, No, No, No, No]
        }
    }

    impl const ReportKeys for [KeyCode; 1] {
        fn keys(self) -> [KeyCode; 6] {
            let [k_0] = self;
            [k_0, No, No, No, No, No]
        }
    }

    impl const ReportKeys for [KeyCode; 2] {
        fn keys(self) -> [KeyCode; 6] {
            let [k_0, k_1] = self;
            [k_0, k_1, No, No, No, No]
        }
    }

    impl const ReportKeys for [KeyCode; 3] {
        fn keys(self) -> [KeyCode; 6] {
            let [k_0, k_1, k_2] = self;
            [k_0, k_1, k_2, No, No, No]
        }
    }

    impl const ReportKeys for [KeyCode; 4] {
        fn keys(self) -> [KeyCode; 6] {
            let [k_0, k_1, k_2, k_3] = self;
            [k_0, k_1, k_2, k_3, No, No]
        }
    }

    impl const ReportKeys for [KeyCode; 5] {
        fn keys(self) -> [KeyCode; 6] {
            let [k_0, k_1, k_2, k_3, k_4] = self;
            [k_0, k_1, k_2, k_3, k_4, No]
        }
    }

    impl const ReportKeys for [KeyCode; 6] {
        fn keys(self) -> [KeyCode; 6] {
            self
        }
    }
}

/// A key code, representing a key in a layout.
#[repr(u8)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, Default)]
pub enum KeyCode {
    /// No key.
    #[default]
    No = 0x00,
    /// Too many keys pressed.
    Phantom = 0x01,
    /// Not used by keebit.
    PostFail = 0x02,
    /// Not used by keebit.
    ErrorUndefined = 0x03,

    /// Keyboard a and A.
    A = 0x04,
    /// Keyboard b and B.
    B = 0x05,
    /// Keyboard c and C.
    C = 0x06,
    /// Keyboard d and D.
    D = 0x07,
    /// Keyboard e and E.
    E = 0x08,
    /// Keyboard f and F.
    F = 0x09,
    /// Keyboard g and G.
    G = 0x0A,
    /// Keyboard h and H.
    H = 0x0B,
    /// Keyboard i and I.
    I = 0x0C,
    /// Keyboard j and J.
    J = 0x0D,
    /// Keyboard k and K.
    K = 0x0E,
    /// Keyboard l and L.
    L = 0x0F,
    /// Keyboard m and M.
    M = 0x10,
    /// Keyboard n and N.
    N = 0x11,
    /// Keyboard o and O.
    O = 0x12,
    /// Keyboard p and P.
    P = 0x13,
    /// Keyboard q and Q.
    Q = 0x14,
    /// Keyboard r and R.
    R = 0x15,
    /// Keyboard s and S.
    S = 0x16,
    /// Keyboard t and T.
    T = 0x17,
    /// Keyboard u and U.
    U = 0x18,
    /// Keyboard v and V.
    V = 0x19,
    /// Keyboard w and W.
    W = 0x1A,
    /// Keyboard x and X.
    X = 0x1B,
    /// Keyboard y and Y.
    Y = 0x1C,
    /// Keyboard z and Z.
    Z = 0x1D,

    /// Keyboard 1 and !
    Kb1 = 0x1E,
    /// Keyboard 2 and @
    Kb2 = 0x1F,
    /// Keyboard 3 and #
    Kb3 = 0x20,
    /// Keyboard 4 and $
    Kb4 = 0x21,
    /// Keyboard 5 and %
    Kb5 = 0x22,
    /// Keyboard 6 and ^
    Kb6 = 0x23,
    /// Keyboard 7 and &
    Kb7 = 0x24,
    /// Keyboard 8 and *
    Kb8 = 0x25,
    /// Keyboard 9 and (
    Kb9 = 0x26,
    /// Keyboard 0 and )
    Kb0 = 0x27,

    /// Keyboard Return (ENTER)
    Enter = 0x28,
    /// Keyboard ESCAPE
    Esc = 0x29,
    /// Keyboard Backspace
    Backspace = 0x2A,
    /// Keyboard Tab
    Tab = 0x2B,
    /// Keyboard Spacebar
    Space = 0x2C,
    /// Keyboard - and _
    Minus = 0x2D,
    /// Keyboard = and +
    Equal = 0x2E,
    /// Keyboard [ and {
    LeftBrace = 0x2F,
    /// Keyboard ] and }
    RightBrace = 0x30,
    /// Keyboard \ and |
    Backslash = 0x31,
    /// Keyboard Non-US # and ~
    HashTilde = 0x32,
    /// Keyboard ; and :
    Semicolon = 0x33,
    /// Keyboard ' and "
    Apostrophe = 0x34,
    /// Keyboard ` and ~
    Grave = 0x35,
    /// Keyboard , and <
    Comma = 0x36,
    /// Keyboard . and >
    Dot = 0x37,
    /// Keyboard / and ?
    Slash = 0x38,
    /// Keyboard Caps Lock
    CapsLock = 0x39,

    /// Keyboard F1
    F1 = 0x3A,
    /// Keyboard F2
    F2 = 0x3B,
    /// Keyboard F3
    F3 = 0x3C,
    /// Keyboard F4
    F4 = 0x3D,
    /// Keyboard F5
    F5 = 0x3E,
    /// Keyboard F6
    F6 = 0x3F,
    /// Keyboard F7
    F7 = 0x40,
    /// Keyboard F8
    F8 = 0x41,
    /// Keyboard F9
    F9 = 0x42,
    /// Keyboard F10
    F10 = 0x43,
    /// Keyboard F11
    F11 = 0x44,
    /// Keyboard F12
    F12 = 0x45,

    /// Keyboard Print Screen
    Sysrq = 0x46,
    /// Keyboard Scroll Lock
    ScrollLock = 0x47,
    /// Keyboard Pause
    Pause = 0x48,
    /// Keyboard Insert
    Insert = 0x49,
    /// Keyboard Home
    Home = 0x4A,
    /// Keyboard Page Up
    PageUp = 0x4B,
    /// Keyboard Delete Forward
    Delete = 0x4C,
    /// Keyboard End
    End = 0x4D,
    /// Keyboard Page Down
    PageDown = 0x4E,
    /// Keyboard Right Arrow
    Right = 0x4F,
    /// Keyboard Left Arrow
    Left = 0x50,
    /// Keyboard Down Arrow
    Down = 0x51,
    /// Keyboard Up Arrow
    Up = 0x52,

    /// Keyboard Num Lock and Clear
    NumLock = 0x53,
    /// Keypad /
    KpSlash = 0x54,
    /// Keypad *
    KpAsterisk = 0x55,
    /// Keypad -
    KpMinus = 0x56,
    /// Keypad +
    KpPlus = 0x57,
    /// Keypad ENTER
    KpEnter = 0x58,
    /// Keypad 1 and End
    Kp1 = 0x59,
    /// Keypad 2 and Down Arrow
    Kp2 = 0x5A,
    /// Keypad 3 and PageDn
    Kp3 = 0x5B,
    /// Keypad 4 and Left Arrow
    Kp4 = 0x5C,
    /// Keypad 5
    Kp5 = 0x5D,
    /// Keypad 6 and Right Arrow
    Kp6 = 0x5E,
    /// Keypad 7 and Home
    Kp7 = 0x5F,
    /// Keypad 8 and Up Arrow
    Kp8 = 0x60,
    /// Keypad 9 and Page Up
    Kp9 = 0x61,
    /// Keypad 0 and Insert
    Kp0 = 0x62,
    /// Keypad . and Delete
    KpDot = 0x63,

    /// Keyboard Non-US \ and | (the key at the right of the shortened left-shift)
    _102ND = 0x64,
    /// Keyboard Application
    Compose = 0x65,
    /// Keyboard Power
    Power = 0x66,
    /// Keypad =
    KpEqual = 0x67,

    /// Keyboard F13
    F13 = 0x68,
    /// Keyboard F14
    F14 = 0x69,
    /// Keyboard F15
    F15 = 0x6A,
    /// Keyboard F16
    F16 = 0x6B,
    /// Keyboard F17
    F17 = 0x6C,
    /// Keyboard F18
    F18 = 0x6D,
    /// Keyboard F19
    F19 = 0x6E,
    /// Keyboard F20
    F20 = 0x6F,
    /// Keyboard F21
    F21 = 0x70,
    /// Keyboard F22
    F22 = 0x71,
    /// Keyboard F23
    F23 = 0x72,
    /// Keyboard F24
    F24 = 0x73,

    /// Keyboard Execute
    Open = 0x74,
    /// Keyboard Help
    Help = 0x75,
    /// Keyboard Menu
    Props = 0x76,
    /// Keyboard Select
    Front = 0x77,
    /// Keyboard Stop
    Stop = 0x78,
    /// Keyboard Again
    Again = 0x79,
    /// Keyboard Undo
    Undo = 0x7A,
    /// Keyboard Cut
    Cut = 0x7B,
    /// Keyboard Copy
    Copy = 0x7C,
    /// Keyboard Paste
    Paste = 0x7D,
    /// Keyboard Find
    Find = 0x7E,
    /// Keyboard Mute
    Mute = 0x7F,
    /// Keyboard Volume Up
    VolumeUp = 0x80,
    /// Keyboard Volume Down
    VolumeDown = 0x81,

    /// Implemented as a locking key; sent as a toggle button. Available for legacy support;
    /// however, most systems should use the non-locking version of this key.
    LockingCapsLock = 0x82,
    /// Implemented as a locking key; sent as a toggle button. Available for legacy support;
    /// however, most systems should use the non-locking version of this key.
    LockingNumLock = 0x83,
    /// Implemented as a locking key; sent as a toggle button. Available for legacy support;
    /// however, most systems should use the non-locking version of this key.
    LockingScrollLock = 0x84,

    /// Keypad Comma is the appropriate usage for the Brazilian keypad period (.) key. This
    /// represents the closest possible match, and system software should do the correct mapping
    /// based on the current locale setting.
    KpComma = 0x85,
    /// Used on AS/400 keyboards.
    KpEqualSign = 0x86,

    /// Keyboard International1 should be identified via footnote as the appropriate usage for the
    /// Brazilian forward-slash (/) and question-mark (?) key. This usage should also be renamed to
    /// either "Keyboard Non-US / and ?" or to "Keyboard International1" now that it’s become clear
    /// that it does not only apply to Kanji keyboards anymore.
    International1 = 0x87,
    /// International 2.
    International2 = 0x88,
    /// International 3.
    International3 = 0x89,
    /// International 4.
    International4 = 0x8A,
    /// International 5.
    International5 = 0x8B,
    /// International 6.
    International6 = 0x8C,
    /// Toggle Double-Byte/Single-Byte mode.
    International7 = 0x8D,
    /// Undefined, available for other Front End Language Processors.
    International8 = 0x8E,
    /// Undefined, available for other Front End Language Processors.
    International9 = 0x8F,

    /// Hangul/English toggle key. This usage is used as an input method editor control key on a
    /// Korean language keyboard.
    Lang1 = 0x90,
    /// Hanja conversion key. This usage is used as an input method editor control key on a Korean
    /// language keyboard.
    Lang2 = 0x91,
    /// Defines the Katakana key for Japanese USB word-processing keyboards.
    Lang3 = 0x92,
    /// Defines the Hiragana key for Japanese USB word-processing keyboards.
    Lang4 = 0x93,
    /// Defines the Zenkaku/Hankaku key for Japanese USB word-processing keyboards.
    Lang5 = 0x94,
    /// Reserved for language-specific functions, such as Front End Processors and Input Method Editors.
    Lang6 = 0x95,
    /// Reserved for language-specific functions, such as Front End Processors and Input Method Editors.
    Lang7 = 0x96,
    /// Reserved for language-specific functions, such as Front End Processors and Input Method Editors.
    Lang8 = 0x97,
    /// Reserved for language-specific functions, such as Front End Processors and Input Method Editors.
    Lang9 = 0x98,

    /// xample, Erase-Eaze™ key.
    AlternateErase = 0x99,
    /// Usage of keys is not modified by the state of the Control, Alt, Shift or Num Lock keys.
    SysReqAttention = 0x9A,
    /// Keyboard Cancel.
    Cancel = 0x9B,
    /// Keyboard Clear.
    Clear = 0x9C,
    /// Keyboard Prior.
    Prior = 0x9D,
    /// Keyboard Return.
    Return = 0x9E,
    /// Keyboard Separator.
    Separator = 0x9F,
    /// Keyboard Out.
    Out = 0xA0,
    /// Keyboard Oper.
    Oper = 0xA1,
    /// Keyboard Clear/Again.
    ClearAgain = 0xA2,
    /// Keyboard CrSel/Props.
    CrSelProps = 0xA3,
    /// Keyboard ExSel.
    ExSel = 0xA4,
    //
    // Reserved: 0xA5-0xAF
    //
    /// Keypad 00
    Kp00 = 0xB0,
    /// Keypad 000
    Kp000 = 0xB1,
    /// The symbol displayed will depend on the current locale settings of the operating system. For
    /// example, the US thousands separator would be a comma, and the decimal separator would be a period.
    ThousandsSeparator = 0xB2,
    /// The symbol displayed will depend on the current locale settings of the operating system. For
    /// example, the US thousands separator would be a comma, and the decimal separator would be a period.
    DecimalSeparator = 0xB3,
    /// The symbol displayed will depend on the current locale settings of the operating system. For
    /// example the US currency unit would be $ and the sub-unit would be ¢.
    CurrencyUnit = 0xB4,
    /// The symbol displayed will depend on the current locale settings of the operating system. For
    /// example the US currency unit would be $ and the sub-unit would be ¢.
    CurrencySubUnit = 0xB5,

    /// Keypad (
    KpLeftParen = 0xB6,
    /// Keypad )
    KpRightParen = 0xB7,
    /// Keypad {
    KpLeftBrace = 0xB8,
    /// Keypad }
    KpRightBrace = 0xB9,
    /// Keypad Tab
    KpTab = 0xBA,
    /// Keypad Backspace
    KpBackspace = 0xBB,
    /// Keypad A
    KpA = 0xBC,
    /// Keypad B
    KpB = 0xBD,
    /// Keypad C
    KpC = 0xBE,
    /// Keypad D
    KpD = 0xBF,
    /// Keypad E
    KpE = 0xC0,
    /// Keypad F
    KpF = 0xC1,
    /// Keypad XOR
    KpXor = 0xC2,
    /// Keypad ∧
    KpExponent = 0xC3,
    /// Keypad %
    KpPercent = 0xC4,
    /// Keypad <
    KpLesser = 0xC5,
    /// Keypad >
    KpGreater = 0xC6,
    /// Keypad &
    KpBitAnd = 0xC7,
    /// Keypad &&
    KpAnd = 0xC8,
    /// Keypad |
    KpBitOr = 0xC9,
    /// Keypad ||
    KpOr = 0xCA,
    /// Keypad :
    KpColon = 0xCB,
    /// Keypad #
    KpSharp = 0xCC,
    /// Keypad Space
    KpSpace = 0xCD,
    /// Keypad @
    KpAt = 0xCE,
    /// Keypad !
    KpNot = 0xCF,

    /// Keypad Memory Store.
    KpMemoryStore = 0xD0,
    /// Keypad Memory Recall.
    KpMemoryRecall = 0xD1,
    /// Keypad Memory Clear.
    KpMemoryClear = 0xD2,
    /// Keypad Memory Add.
    KpMemoryAdd = 0xD3,
    /// Keypad Memory Subtract.
    KpMemorySubtract = 0xD4,
    /// Keypad Memory Multiply.
    KpMemoryMultiply = 0xD5,
    /// Keypad Memory Divide.
    KpMemoryDivide = 0xD6,
    /// Keypad +/-.
    KpPlusOrMinus = 0xD7,
    /// Keypad Clear.
    KpClear = 0xD8,
    /// Keypad Clear Entry.
    KpClearEntry = 0xD9,
    /// Keypad Binary.
    KpBinary = 0xDA,
    /// Keypad Octal.
    KpOctal = 0xDB,
    /// Keypad Decimal.
    KpDecimal = 0xDC,
    /// Keypad Hexadecimal.
    KpHexadecimal = 0xDD,
    //
    // Reserved: 0xDE-0xDF
    //
    /// Keyboard LeftControl.
    LeftControl = 0xE0,
    /// Keyboard LeftShift.
    LeftShift = 0xE1,
    /// Keyboard LeftAlt.
    LeftAlt = 0xE2,
    /// Keyboard LeftGui.
    ///
    /// Windows key, and Compose.
    /// Windowing environment key, examples are Microsoft Win key, Mac Apple key, Sun Meta key.
    LeftGui = 0xE3,
    /// Keyboard RightControl.
    RightControl = 0xE4,
    /// Keyboard RightShift.
    RightShift = 0xE5,
    /// Keyboard RightAlt.
    RightAlt = 0xE6,
    /// Keyboard RightGui.
    ///
    /// Windows key, and Compose.
    /// Windowing environment key, examples are Microsoft Win key, Mac Apple key, Sun Meta key.
    RightGui = 0xE7,
}

impl Report {
    /// Create a new empty `Report`.
    pub const fn empty() -> Self {
        Report {
            modifiers: ModifiersByte(0),
            #[cfg(feature = "reserved")]
            reserved: ReservedByte(0),
            keypresses: [KeyCode::No; 6],
        }
    }

    /// Returns a new builder to create a report in a typesafe manner. Only the
    /// correct amount of keys can be added.
    pub const fn builder() -> ReportBuilder<0> {
        ReportBuilder(Report::empty())
    }

    /// Create a new report with the given modifiers and key code.
    pub fn from_parts(
        modifiers: impl Into<ModifiersByte>,
        keypresses: impl sealed::ReportKeys,
    ) -> Self {
        Report {
            modifiers: modifiers.into(),
            #[cfg(feature = "reserved")]
            reserved: Default::default(),
            keypresses: keypresses.keys(),
        }
    }

    /// Create a rollover report, indicating too many keys have been pressed.
    pub const fn rollover() -> Self {
        Report {
            keypresses: [KeyCode::Phantom; 6],
            ..Self::empty()
        }
    }

    /// Returns the key at the given index, or [`No`](KeyCode::No) if it is out of bounds.
    pub fn key(&self, index: usize) -> KeyCode {
        self.keypresses.get(index).copied().unwrap_or(KeyCode::No)
    }

    /// Returns the set keys from the report.
    pub fn iter_keys(&self) -> impl Iterator<Item = KeyCode> + '_ {
        self.keypresses
            .iter()
            .copied()
            .filter(|&k| k != KeyCode::No)
    }

    /// Returns the set keys from the report.
    pub fn keys_mut(&mut self) -> &mut [KeyCode; 6] {
        &mut self.keypresses
    }

    /// Pushes a new key in the report, if there is enough space remaining.
    pub fn add_key(&mut self, key: KeyCode) {
        if let Some(slot) = self.keypresses.iter_mut().find(|k| **k == KeyCode::No) {
            *slot = key;
        }
        //TODO: should I use rollover like that?
        // Perhaps it should be set up in a config field

        //else {
        //    self.keypresses = [KeyCode::Phantom; 6];
        //}
    }

    /// Removes a key from the report.
    pub fn remove_key(&mut self, key: KeyCode) {
        if let Some(index) = self.keypresses.iter().position(|k| *k == key) {
            self.keypresses.copy_within((index + 1)..6, index);
            self.keypresses[5] = KeyCode::No;
        }
    }

    /// Add modifiers in the report.
    pub fn add_mod(&mut self, mods: ModifiersByte) {
        self.modifiers.0 |= u8::from(mods);
    }

    /// Creates a new report, with the given key added.
    pub fn with_key(mut self, key: KeyCode) -> Self {
        self.add_key(key);
        self
    }

    /// Creates a new report, with the given modifers added.
    pub fn with_mod(self, mods: ModifiersByte) -> Self {
        Self {
            modifiers: ModifiersByte(self.modifiers.0 | mods.0),
            ..self
        }
    }

    /// Creates a new report, with the reserved byte being the one provided.
    #[cfg(feature = "reserved")]
    pub fn with_reserved_bits(self, byte: reserved_byte::ReservedByte) -> Self {
        Self {
            reserved: byte,
            ..self
        }
    }

    /// Creates a new report, with given reserved bits being activated.
    #[cfg(feature = "reserved")]
    pub fn with_activated_reserved_bits(mut self, byte: reserved_byte::ReservedByte) -> Self {
        self.set_activated_reserved_bits(byte);
        self
    }

    /// Creates a new report, with given reserved bits being deactivated.
    #[cfg(feature = "reserved")]
    pub fn with_deactivated_reserved_bits(mut self, byte: reserved_byte::ReservedByte) -> Self {
        self.set_deactivated_reserved_bits(byte);
        self
    }

    /// Sets the report, with given reserved bits being activated.
    #[cfg(feature = "reserved")]
    pub fn set_activated_reserved_bits(&mut self, byte: reserved_byte::ReservedByte) {
        self.reserved.0 |= byte.0;
    }

    /// sets the report, with given reserved bits being deactivated.
    #[cfg(feature = "reserved")]
    pub fn set_deactivated_reserved_bits(&mut self, byte: reserved_byte::ReservedByte) {
        self.reserved.0 = nimply(self.reserved.0, byte.0);
    }

    /// Serializes the report in bytes to be sent to the host.
    pub fn into_bytes(self) -> [u8; 8] {
        #[cfg(not(feature = "reserved"))]
        let reserved = 0;
        #[cfg(feature = "reserved")]
        let reserved = self.reserved.0;

        [
            self.modifiers.into(),
            reserved,
            self.keypresses[0].into(),
            self.keypresses[1].into(),
            self.keypresses[2].into(),
            self.keypresses[3].into(),
            self.keypresses[4].into(),
            self.keypresses[5].into(),
        ]
    }

    /// The number of keys into the report (from 0 to 6).
    ///
    /// ```
    /// # use keebit::prelude::*;
    /// let report = Report::default()
    ///     .with_key(A)
    ///     .with_key(B)
    ///     .with_key(C)
    ///     .with_key(D);
    ///
    /// assert_eq!(report.len(), 4);
    /// ```
    pub fn len(&self) -> usize {
        self.keypresses
            .iter()
            .take_while(|&&k| k != KeyCode::No)
            .count()
    }

    /// The number of free key slots into the report (from 0 to 6).
    ///
    /// ```
    /// # use keebit::prelude::*;
    /// let report = Report::default()
    ///     .with_key(A)
    ///     .with_key(B)
    ///     .with_key(C)
    ///     .with_key(D);
    ///
    /// assert_eq!(report.free(), 2);
    /// ```
    pub fn free(&self) -> usize {
        self.keypresses
            .iter()
            .rev()
            .take_while(|&&k| k == KeyCode::No)
            .count()
    }

    /// Returns if there are no leys in the report.
    ///
    /// ```
    /// # use keebit::prelude::*;
    /// let report = Report::default();
    ///
    /// assert_eq!(report.is_empty(), true);
    /// ```
    pub fn is_empty(&self) -> bool {
        self.keypresses[0] == KeyCode::No
    }
}

impl ModifiersByte {
    /// Returns if there is no modifiers in the structure, *i.e.* the value is 0.
    pub const fn is_none(self) -> bool {
        self.0 == 0
    }
}

/* IMPLS */

#[cfg(feature = "reserved")]
pub(crate) mod reserved_byte {
    use super::*;

    /// The reserved byte in the HID report. Activated with the `reserved` feature.
    ///
    /// It can be used to hack the HID protocol and include some custom data, notably
    /// done by Apple with the “fn” key.
    #[derive(Debug, Default, PartialEq, Clone, Copy)]
    pub struct ReservedByte(pub u8);

    impl ReservedByte {
        /// Normal value for the reserved byte: `ReservedByte(0)`.
        pub const ZERO: Self = Self(0);
    }

    impl From<ReservedByte> for ModifiersBuilder {
        fn from(reserved: ReservedByte) -> Self {
            ModifiersBuilder {
                modifiers: Default::default(),
                reserved,
            }
        }
    }

    // ReservedByte + ReservedByte

    impl Add for ReservedByte {
        type Output = ReservedByte;

        fn add(self, rhs: ReservedByte) -> Self::Output {
            ReservedByte(self.0 | rhs.0)
        }
    }

    // ModifiersByte + ReservedByte

    impl Add<ReservedByte> for ModifiersByte {
        type Output = ModifiersBuilder;

        fn add(self, reserved: ReservedByte) -> Self::Output {
            ModifiersBuilder {
                modifiers: self,
                reserved,
            }
        }
    }

    impl Add<ModifiersByte> for ReservedByte {
        type Output = ModifiersBuilder;

        fn add(self, modifiers: ModifiersByte) -> Self::Output {
            ModifiersBuilder {
                modifiers,
                reserved: self,
            }
        }
    }

    // ModifiersBuilder + ReservedByte

    impl Add<ModifiersBuilder> for ReservedByte {
        type Output = ModifiersBuilder;

        fn add(
            self,
            ModifiersBuilder {
                modifiers,
                reserved,
            }: ModifiersBuilder,
        ) -> Self::Output {
            ModifiersBuilder {
                modifiers,
                reserved: ReservedByte(self.0 | reserved.0),
            }
        }
    }

    impl Add<ReservedByte> for ModifiersBuilder {
        type Output = ModifiersBuilder;

        fn add(self, reserved: ReservedByte) -> Self::Output {
            ModifiersBuilder {
                modifiers: self.modifiers,
                reserved: ReservedByte(self.reserved.0 | reserved.0),
            }
        }
    }
}

mod modifiers_builder {
    use super::*;

    /// An intermediate type to hold the modifiers byte and the reserved one.
    ///
    /// Useful when MacOS users need to have `Fn + Command` for example.
    #[derive(Debug)]
    pub struct ModifiersBuilder {
        pub(crate) modifiers: ModifiersByte,
        #[cfg(feature = "reserved")]
        pub(crate) reserved: ReservedByte,
    }

    impl From<ModifiersByte> for ModifiersBuilder {
        fn from(modifiers: ModifiersByte) -> Self {
            ModifiersBuilder {
                modifiers,
                #[cfg(feature = "reserved")]
                reserved: Default::default(),
            }
        }
    }

    // ModifiersBuilder + ModifiersByte

    impl Add<ModifiersBuilder> for ModifiersByte {
        type Output = ModifiersBuilder;

        fn add(
            self,
            ModifiersBuilder {
                modifiers,
                #[cfg(feature = "reserved")]
                reserved,
            }: ModifiersBuilder,
        ) -> Self::Output {
            ModifiersBuilder {
                modifiers: modifiers + self,
                #[cfg(feature = "reserved")]
                reserved,
            }
        }
    }

    impl Add<ModifiersByte> for ModifiersBuilder {
        type Output = ModifiersBuilder;

        fn add(self, modifiers: ModifiersByte) -> Self::Output {
            ModifiersBuilder {
                modifiers: self.modifiers + modifiers,
                #[cfg(feature = "reserved")]
                reserved: self.reserved,
            }
        }
    }

    // ModifiersBuilder + ModifiersBuilder

    impl Add for ModifiersBuilder {
        type Output = ModifiersBuilder;

        fn add(self, rhs: Self) -> Self::Output {
            ModifiersBuilder {
                modifiers: self.modifiers + rhs.modifiers,
                #[cfg(feature = "reserved")]
                reserved: ReservedByte(self.reserved.0 | rhs.reserved.0),
            }
        }
    }
}

mod report_builder {
    use super::{sealed::ReportKeys, KeyCode, ModifiersByte, Report};
    use core::ops::Add;

    /// An intermediate type to hold the modifiers byte and the reserved one.
    ///
    /// Useful when MacOS users need to have `Fn + Command` for example.
    #[derive(Debug)]
    pub struct ReportBuilder<const N: usize>(pub(super) Report);

    impl<const N: usize> ReportBuilder<N> {
        /// Adds a new modifier to the report.
        pub const fn m(mut self, modifier: ModifiersByte) -> Self {
            self.0.modifiers.0 |= modifier.0;
            self
        }

        /// Returns the report.
        pub const fn build(self) -> Report {
            self.0
        }
    }

    impl ReportBuilder<0> {
        /// Adds a new key to the report.
        pub const fn k(self, key: KeyCode) -> ReportBuilder<1> {
            ReportBuilder(Report {
                modifiers: ModifiersByte(0),
                #[cfg(feature = "reserved")]
                reserved: ReservedByte(0),
                keypresses: [
                    key,
                    KeyCode::No,
                    KeyCode::No,
                    KeyCode::No,
                    KeyCode::No,
                    KeyCode::No,
                ],
            })
        }
    }

    impl ReportBuilder<1> {
        /// Adds a new key to the report.
        pub const fn k(self, key: KeyCode) -> ReportBuilder<2> {
            let mut report = self.0;
            report.keypresses[1] = key;
            ReportBuilder(report)
        }
    }

    impl ReportBuilder<2> {
        /// Adds a new key to the report.
        pub const fn k(self, key: KeyCode) -> ReportBuilder<3> {
            let mut report = self.0;
            report.keypresses[2] = key;
            ReportBuilder(report)
        }
    }

    impl ReportBuilder<3> {
        /// Adds a new key to the report.
        pub const fn k(self, key: KeyCode) -> ReportBuilder<4> {
            let mut report = self.0;
            report.keypresses[3] = key;
            ReportBuilder(report)
        }
    }

    impl ReportBuilder<4> {
        /// Adds a new key to the report.
        pub const fn k(self, key: KeyCode) -> ReportBuilder<5> {
            let mut report = self.0;
            report.keypresses[4] = key;
            ReportBuilder(report)
        }
    }

    impl ReportBuilder<5> {
        /// Adds a new key to the report.
        pub const fn k(self, key: KeyCode) -> ReportBuilder<6> {
            let mut report = self.0;
            report.keypresses[5] = key;
            ReportBuilder(report)
        }
    }

    impl Add<KeyCode> for ModifiersByte {
        type Output = ReportBuilder<1>;

        fn add(self, keycode: KeyCode) -> Self::Output {
            ReportBuilder(Report {
                modifiers: self,
                #[cfg(feature = "reserved")]
                reserved: Default::default(),
                keypresses: keycode.keys(),
            })
        }
    }

    #[cfg(feature = "reserved")]
    impl Add<KeyCode> for super::ReservedByte {
        type Output = ReportBuilder<1>;

        fn add(self, keycode: KeyCode) -> Self::Output {
            ReportBuilder(Report {
                modifiers: Default::default(),
                reserved: self,
                keypresses: keycode.keys(),
            })
        }
    }

    #[cfg(feature = "reserved")]
    impl Add<KeyCode> for super::ModifiersBuilder {
        type Output = ReportBuilder<1>;

        fn add(self, keycode: KeyCode) -> Self::Output {
            ReportBuilder(Report {
                modifiers: self.modifiers,
                reserved: self.reserved,
                keypresses: keycode.keys(),
            })
        }
    }

    impl Add<KeyCode> for KeyCode {
        type Output = ReportBuilder<2>;

        fn add(self, rhs: KeyCode) -> Self::Output {
            ReportBuilder(Report::default().with_key(self).with_key(rhs))
        }
    }

    impl Add<KeyCode> for ReportBuilder<1> {
        type Output = ReportBuilder<2>;

        fn add(self, rhs: KeyCode) -> Self::Output {
            ReportBuilder(self.0.with_key(rhs))
        }
    }

    impl Add<KeyCode> for ReportBuilder<2> {
        type Output = ReportBuilder<3>;

        fn add(self, rhs: KeyCode) -> Self::Output {
            ReportBuilder(self.0.with_key(rhs))
        }
    }

    impl Add<KeyCode> for ReportBuilder<3> {
        type Output = ReportBuilder<4>;

        fn add(self, rhs: KeyCode) -> Self::Output {
            ReportBuilder(self.0.with_key(rhs))
        }
    }

    impl Add<KeyCode> for ReportBuilder<4> {
        type Output = ReportBuilder<5>;

        fn add(self, rhs: KeyCode) -> Self::Output {
            ReportBuilder(self.0.with_key(rhs))
        }
    }

    impl Add<KeyCode> for ReportBuilder<5> {
        type Output = ReportBuilder<6>;

        fn add(self, rhs: KeyCode) -> Self::Output {
            ReportBuilder(self.0.with_key(rhs))
        }
    }
}

impl From<ModifiersByte> for u8 {
    fn from(modifier: ModifiersByte) -> u8 {
        modifier.0
    }
}

impl From<KeyCode> for u8 {
    fn from(key: KeyCode) -> u8 {
        key as u8
    }
}

impl From<KeyCode> for Report {
    fn from(key: KeyCode) -> Report {
        Report {
            modifiers: Default::default(),
            #[cfg(feature = "reserved")]
            reserved: Default::default(),
            keypresses: key.keys(),
        }
    }
}

impl Add for ModifiersByte {
    type Output = ModifiersByte;

    fn add(self, rhs: ModifiersByte) -> Self::Output {
        #[allow(clippy::suspicious_arithmetic_impl)]
        ModifiersByte(self.0 | rhs.0)
    }
}

impl AddAssign for ModifiersByte {
    fn add_assign(&mut self, rhs: Self) {
        *self = *self + rhs
    }
}

impl Sub for ModifiersByte {
    type Output = ModifiersByte;

    fn sub(self, rhs: ModifiersByte) -> Self::Output {
        ModifiersByte(nimply(self.0, rhs.0))
    }
}

impl SubAssign for ModifiersByte {
    fn sub_assign(&mut self, rhs: Self) {
        *self = *self - rhs
    }
}

impl TryFrom<u8> for KeyCode {
    type Error = ();

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0x00 => Ok(Self::No),
            0x01 => Ok(Self::Phantom),
            0x02 => Ok(Self::PostFail),
            0x03 => Ok(Self::ErrorUndefined),

            0x04 => Ok(Self::A),
            0x05 => Ok(Self::B),
            0x06 => Ok(Self::C),
            0x07 => Ok(Self::D),
            0x08 => Ok(Self::E),
            0x09 => Ok(Self::F),
            0x0A => Ok(Self::G),
            0x0B => Ok(Self::H),
            0x0C => Ok(Self::I),
            0x0D => Ok(Self::J),
            0x0E => Ok(Self::K),
            0x0F => Ok(Self::L),
            0x10 => Ok(Self::M),
            0x11 => Ok(Self::N),
            0x12 => Ok(Self::O),
            0x13 => Ok(Self::P),
            0x14 => Ok(Self::Q),
            0x15 => Ok(Self::R),
            0x16 => Ok(Self::S),
            0x17 => Ok(Self::T),
            0x18 => Ok(Self::U),
            0x19 => Ok(Self::V),
            0x1A => Ok(Self::W),
            0x1B => Ok(Self::X),
            0x1C => Ok(Self::Y),
            0x1D => Ok(Self::Z),

            0x1E => Ok(Self::Kb1),
            0x1F => Ok(Self::Kb2),
            0x20 => Ok(Self::Kb3),
            0x21 => Ok(Self::Kb4),
            0x22 => Ok(Self::Kb5),
            0x23 => Ok(Self::Kb6),
            0x24 => Ok(Self::Kb7),
            0x25 => Ok(Self::Kb8),
            0x26 => Ok(Self::Kb9),
            0x27 => Ok(Self::Kb0),

            0x28 => Ok(Self::Enter),
            0x29 => Ok(Self::Esc),
            0x2A => Ok(Self::Backspace),
            0x2B => Ok(Self::Tab),
            0x2C => Ok(Self::Space),
            0x2D => Ok(Self::Minus),
            0x2E => Ok(Self::Equal),
            0x2F => Ok(Self::LeftBrace),
            0x30 => Ok(Self::RightBrace),
            0x31 => Ok(Self::Backslash),
            0x32 => Ok(Self::HashTilde),
            0x33 => Ok(Self::Semicolon),
            0x34 => Ok(Self::Apostrophe),
            0x35 => Ok(Self::Grave),
            0x36 => Ok(Self::Comma),
            0x37 => Ok(Self::Dot),
            0x38 => Ok(Self::Slash),
            0x39 => Ok(Self::CapsLock),

            0x3A => Ok(Self::F1),
            0x3B => Ok(Self::F2),
            0x3C => Ok(Self::F3),
            0x3D => Ok(Self::F4),
            0x3E => Ok(Self::F5),
            0x3F => Ok(Self::F6),
            0x40 => Ok(Self::F7),
            0x41 => Ok(Self::F8),
            0x42 => Ok(Self::F9),
            0x43 => Ok(Self::F10),
            0x44 => Ok(Self::F11),
            0x45 => Ok(Self::F12),

            0x46 => Ok(Self::Sysrq),
            0x47 => Ok(Self::ScrollLock),
            0x48 => Ok(Self::Pause),
            0x49 => Ok(Self::Insert),
            0x4A => Ok(Self::Home),
            0x4B => Ok(Self::PageUp),
            0x4C => Ok(Self::Delete),
            0x4D => Ok(Self::End),
            0x4E => Ok(Self::PageDown),
            0x4F => Ok(Self::Right),
            0x50 => Ok(Self::Left),
            0x51 => Ok(Self::Down),
            0x52 => Ok(Self::Up),

            0x53 => Ok(Self::NumLock),
            0x54 => Ok(Self::KpSlash),
            0x55 => Ok(Self::KpAsterisk),
            0x56 => Ok(Self::KpMinus),
            0x57 => Ok(Self::KpPlus),
            0x58 => Ok(Self::KpEnter),
            0x59 => Ok(Self::Kp1),
            0x5A => Ok(Self::Kp2),
            0x5B => Ok(Self::Kp3),
            0x5C => Ok(Self::Kp4),
            0x5D => Ok(Self::Kp5),
            0x5E => Ok(Self::Kp6),
            0x5F => Ok(Self::Kp7),
            0x60 => Ok(Self::Kp8),
            0x61 => Ok(Self::Kp9),
            0x62 => Ok(Self::Kp0),
            0x63 => Ok(Self::KpDot),

            0x64 => Ok(Self::_102ND),
            0x65 => Ok(Self::Compose),
            0x66 => Ok(Self::Power),
            0x67 => Ok(Self::KpEqual),

            0x68 => Ok(Self::F13),
            0x69 => Ok(Self::F14),
            0x6A => Ok(Self::F15),
            0x6B => Ok(Self::F16),
            0x6C => Ok(Self::F17),
            0x6D => Ok(Self::F18),
            0x6E => Ok(Self::F19),
            0x6F => Ok(Self::F20),
            0x70 => Ok(Self::F21),
            0x71 => Ok(Self::F22),
            0x72 => Ok(Self::F23),
            0x73 => Ok(Self::F24),

            0x74 => Ok(Self::Open),
            0x75 => Ok(Self::Help),
            0x76 => Ok(Self::Props),
            0x77 => Ok(Self::Front),
            0x78 => Ok(Self::Stop),
            0x79 => Ok(Self::Again),
            0x7A => Ok(Self::Undo),
            0x7B => Ok(Self::Cut),
            0x7C => Ok(Self::Copy),
            0x7D => Ok(Self::Paste),
            0x7E => Ok(Self::Find),
            0x7F => Ok(Self::Mute),
            0x80 => Ok(Self::VolumeUp),
            0x81 => Ok(Self::VolumeDown),

            0x82 => Ok(Self::LockingCapsLock),
            0x83 => Ok(Self::LockingNumLock),
            0x84 => Ok(Self::LockingScrollLock),

            0x85 => Ok(Self::KpComma),
            0x86 => Ok(Self::KpEqualSign),

            0x87 => Ok(Self::International1),
            0x88 => Ok(Self::International2),
            0x89 => Ok(Self::International3),
            0x8A => Ok(Self::International4),
            0x8B => Ok(Self::International5),
            0x8C => Ok(Self::International6),
            0x8D => Ok(Self::International7),
            0x8E => Ok(Self::International8),
            0x8F => Ok(Self::International9),

            0x90 => Ok(Self::Lang1),
            0x91 => Ok(Self::Lang2),
            0x92 => Ok(Self::Lang3),
            0x93 => Ok(Self::Lang4),
            0x94 => Ok(Self::Lang5),
            0x95 => Ok(Self::Lang6),
            0x96 => Ok(Self::Lang7),
            0x97 => Ok(Self::Lang8),
            0x98 => Ok(Self::Lang9),

            0x99 => Ok(Self::AlternateErase),
            0x9A => Ok(Self::SysReqAttention),
            0x9B => Ok(Self::Cancel),
            0x9C => Ok(Self::Clear),
            0x9D => Ok(Self::Prior),
            0x9E => Ok(Self::Return),
            0x9F => Ok(Self::Separator),
            0xA0 => Ok(Self::Out),
            0xA1 => Ok(Self::Oper),
            0xA2 => Ok(Self::ClearAgain),
            0xA3 => Ok(Self::CrSelProps),
            0xA4 => Ok(Self::ExSel),

            0xB0 => Ok(Self::Kp00),
            0xB1 => Ok(Self::Kp000),
            0xB2 => Ok(Self::ThousandsSeparator),
            0xB3 => Ok(Self::DecimalSeparator),
            0xB4 => Ok(Self::CurrencyUnit),
            0xB5 => Ok(Self::CurrencySubUnit),

            0xB6 => Ok(Self::KpLeftParen),
            0xB7 => Ok(Self::KpRightParen),
            0xB8 => Ok(Self::KpLeftBrace),
            0xB9 => Ok(Self::KpRightBrace),
            0xBA => Ok(Self::KpTab),
            0xBB => Ok(Self::KpBackspace),
            0xBC => Ok(Self::KpA),
            0xBD => Ok(Self::KpB),
            0xBE => Ok(Self::KpC),
            0xBF => Ok(Self::KpD),
            0xC0 => Ok(Self::KpE),
            0xC1 => Ok(Self::KpF),
            0xC2 => Ok(Self::KpXor),
            0xC3 => Ok(Self::KpExponent),
            0xC4 => Ok(Self::KpPercent),
            0xC5 => Ok(Self::KpLesser),
            0xC6 => Ok(Self::KpGreater),
            0xC7 => Ok(Self::KpBitAnd),
            0xC8 => Ok(Self::KpAnd),
            0xC9 => Ok(Self::KpBitOr),
            0xCA => Ok(Self::KpOr),
            0xCB => Ok(Self::KpColon),
            0xCC => Ok(Self::KpSharp),
            0xCD => Ok(Self::KpSpace),
            0xCE => Ok(Self::KpAt),
            0xCF => Ok(Self::KpNot),

            0xD0 => Ok(Self::KpMemoryStore),
            0xD1 => Ok(Self::KpMemoryRecall),
            0xD2 => Ok(Self::KpMemoryClear),
            0xD3 => Ok(Self::KpMemoryAdd),
            0xD4 => Ok(Self::KpMemorySubtract),
            0xD5 => Ok(Self::KpMemoryMultiply),
            0xD6 => Ok(Self::KpMemoryDivide),
            0xD7 => Ok(Self::KpPlusOrMinus),
            0xD8 => Ok(Self::KpClear),
            0xD9 => Ok(Self::KpClearEntry),
            0xDA => Ok(Self::KpBinary),
            0xDB => Ok(Self::KpOctal),
            0xDC => Ok(Self::KpDecimal),
            0xDD => Ok(Self::KpHexadecimal),

            0xE0 => Ok(Self::LeftControl),
            0xE1 => Ok(Self::LeftShift),
            0xE2 => Ok(Self::LeftAlt),
            0xE3 => Ok(Self::LeftGui),
            0xE4 => Ok(Self::RightControl),
            0xE5 => Ok(Self::RightShift),
            0xE6 => Ok(Self::RightAlt),
            0xE7 => Ok(Self::RightGui),

            _not_a_variant => Err(()),
        }
    }
}

use core::fmt;

use self::sealed::ReportKeys;

impl fmt::Debug for Report {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "<")?;
        if self.modifiers.0 & u8::from(LeftCtrl) != 0 {
            write!(f, "LeftCtrl + ")?;
        }
        if self.modifiers.0 & u8::from(LeftShift) != 0 {
            write!(f, "LeftShift + ")?;
        }
        if self.modifiers.0 & u8::from(LeftAlt) != 0 {
            write!(f, "LeftAlt + ")?;
        }
        if self.modifiers.0 & u8::from(LeftGui) != 0 {
            write!(f, "LeftGui + ")?;
        }
        if self.modifiers.0 & u8::from(RightCtrl) != 0 {
            write!(f, "RightCtrl + ")?;
        }
        if self.modifiers.0 & u8::from(RightShift) != 0 {
            write!(f, "RightShift + ")?;
        }
        if self.modifiers.0 & u8::from(RightAlt) != 0 {
            write!(f, "RightAlt + ")?;
        }
        if self.modifiers.0 & u8::from(RightGui) != 0 {
            write!(f, "RightGui + ")?;
        }

        #[cfg(feature = "reserved")]
        write!(f, "{:?} + ", self.reserved)?;

        write!(f, "{:?}", self.keypresses[0])?;

        for k in self.keypresses[1..]
            .iter()
            .take_while(|&&k| k != KeyCode::No)
        {
            write!(f, ",{k:?}")?;
        }

        write!(f, ">")?;

        Ok(())
    }
}

impl fmt::Debug for ModifiersByte {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut byte = self.0;

        write!(f, "<")?;

        if byte == 1 {
            write!(f, "LeftCtrl")?;
        } else if byte & 1 != 0 {
            write!(f, "LeftCtrl + ")?;
        }

        byte >>= 1;
        if byte == 1 {
            write!(f, "LeftShift")?;
        } else if byte & 1 != 0 {
            write!(f, "LeftShift + ")?;
        }

        byte >>= 1;
        if byte == 1 {
            write!(f, "LeftAlt")?;
        } else if byte & 1 != 0 {
            write!(f, "LeftAlt + ")?;
        }

        byte >>= 1;
        if byte == 1 {
            write!(f, "LeftGui")?;
        } else if byte & 1 != 0 {
            write!(f, "LeftGui + ")?;
        }

        byte >>= 1;
        if byte == 1 {
            write!(f, "RightCtrl")?;
        } else if byte & 1 != 0 {
            write!(f, "RightCtrl + ")?;
        }

        byte >>= 1;
        if byte == 1 {
            write!(f, "RightShift")?;
        } else if byte & 1 != 0 {
            write!(f, "RightShift + ")?;
        }

        byte >>= 1;
        if byte == 1 {
            write!(f, "RightAlt")?;
        } else if byte & 1 != 0 {
            write!(f, "RightAlt + ")?;
        }

        byte >>= 1;
        if byte == 1 {
            write!(f, "RightGui")?;
        } else if byte & 1 != 0 {
            write!(f, "RightGui + ")?;
        }

        write!(f, ">")
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use KeyCode::*;

    #[test]
    fn remove_key_when_empty_does_nothing() {
        let mut report = Report::default().with_mod(LeftAlt);
        let report_ = report.clone();

        report.remove_key(A);

        assert_eq!(report, report_);
    }

    #[test]
    fn remove_key_at_the_end_works() {
        let mut report = Report::default().with_mod(LeftAlt);

        report.remove_key(A);

        assert_eq!(report, Report::default().with_mod(LeftAlt));
    }

    #[test]
    fn remove_unique_key_works() {
        let mut report = Report::default().with_mod(LeftAlt).with_key(A);

        report.remove_key(A);

        assert_eq!(report, Report::default().with_mod(LeftAlt));
    }

    #[test]
    fn remove_first_key_works() {
        let mut report = Report::default()
            .with_mod(LeftAlt)
            .with_key(A)
            .with_key(B)
            .with_key(C)
            .with_key(D)
            .with_key(E)
            .with_key(F);

        report.remove_key(A);

        assert_eq!(
            report,
            Report::default()
                .with_mod(LeftAlt)
                .with_key(B)
                .with_key(C)
                .with_key(D)
                .with_key(E)
                .with_key(F)
        );
    }

    #[test]
    fn remove_middle_key_works() {
        let mut report = Report::default()
            .with_mod(LeftAlt)
            .with_key(A)
            .with_key(B)
            .with_key(C)
            .with_key(D)
            .with_key(E)
            .with_key(F);

        report.remove_key(C);

        assert_eq!(
            report,
            Report::default()
                .with_mod(LeftAlt)
                .with_key(A)
                .with_key(B)
                .with_key(D)
                .with_key(E)
                .with_key(F)
        );
    }

    #[test]
    fn remove_last_key_works() {
        let mut report = Report::default()
            .with_mod(LeftAlt)
            .with_key(A)
            .with_key(B)
            .with_key(C)
            .with_key(D)
            .with_key(E)
            .with_key(F);

        report.remove_key(F);

        assert_eq!(
            report,
            Report::default()
                .with_mod(LeftAlt)
                .with_key(A)
                .with_key(B)
                .with_key(C)
                .with_key(D)
                .with_key(E)
        );
    }
}
