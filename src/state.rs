/// Everything to keep track of the hardware state, *i.e.* if the keys are
/// pressed or not, and if events are to be generated.
use crate::coord::Coord;
use core::{fmt, iter::zip, ops::Not as _};

/// The kind of a key event: pressed or released.
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum EventKind {
    /// The key has been pressed.
    Pressed,
    /// The key has been released.
    Released,
}

/// The event is either a regular key event, or a chord event.
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum EventPlace {
    /// The key pressed is a regular one, on the grid.
    Key(Coord),
    /// A chord has been pressed, *i.e.* a series of keys configured to form a single action.
    Chord(usize),
}

/// A keyboard event, with a kind (pressed, released, timeout) and a
/// place (layer coordinates or chord).
#[derive(Clone, Copy)]
pub struct Event {
    /// The event kind (pressed, released, timeout).
    pub kind: EventKind,
    /// The event location (layer coordinates or chord).
    pub place: EventPlace,
}

impl Event {
    /// Returns `true` if it is a pressed event.
    pub fn is_pressed(self) -> bool {
        self.kind == EventKind::Pressed
    }

    /// Returns `true` if it is a released event.
    pub fn is_released(self) -> bool {
        self.kind == EventKind::Released
    }
}

/// Handles the hardware keyboard state (pressed, not pressed) as well as what
/// is used for the debouncing.
#[derive(Debug)]
pub struct StateHandler<const ROW: usize, const COL: usize> {
    previous_state: [[bool; COL]; ROW],
    current_state: [[bool; COL]; ROW],
    timeout: u64,
}

impl<const ROW: usize, const COL: usize> StateHandler<ROW, COL> {
    pub fn new(timeout: u64) -> Self {
        StateHandler {
            previous_state: [[false; COL]; ROW],
            current_state: [[false; COL]; ROW],
            timeout,
        }
    }

    pub fn events(
        &mut self,
        new_state: [[bool; COL]; ROW],
        now: u64,
        debounce_time: u64,
    ) -> impl Iterator<Item = Event> + '_ {
        core::iter::from_coroutine(move || {
            if debounce_time != 0 {
                if new_state != self.current_state {
                    self.current_state = new_state;
                    self.timeout = now + debounce_time;
                    return;
                }
                if now < self.timeout {
                    return;
                }
            }
            for (row, (prev_row, new_row)) in zip(self.previous_state, new_state).enumerate() {
                for (col, (prev_key, new_key)) in zip(prev_row, new_row).enumerate() {
                    if prev_key && new_key.not() {
                        yield Event {
                            place: EventPlace::Key(Coord::new(row as u8, col as u8)),
                            kind: EventKind::Released,
                        };
                    } else if prev_key.not() && new_key {
                        yield Event {
                            place: EventPlace::Key(Coord::new(row as u8, col as u8)),
                            kind: EventKind::Pressed,
                        };
                    }
                }
            }
            self.previous_state = new_state;
            if debounce_time != 0 {
                self.current_state = new_state;
            }
        })
    }
}

impl fmt::Debug for Event {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.kind {
            EventKind::Pressed => write!(f, "<Key Pressed at "),
            EventKind::Released => write!(f, "<Key Released at "),
        }?;
        match self.place {
            EventPlace::Key(coord) => write!(f, "{:?}>", coord),
            EventPlace::Chord(chord) => write!(f, "Chord({:?})>", chord),
        }
    }
}
